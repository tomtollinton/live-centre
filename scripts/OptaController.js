"use strict";

var hl = require ( 'highland' );
var R = require ( 'ramda' );
var AWS = require( 'aws-sdk' );
var s3 = new AWS.S3({
    "accessKeyId": "AKIAIM5MRBX2KZ25JEPQ",
    "secretAccessKey": "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",
    "region": "eu-west-1"
});

module.exports = {
    getFeed : (req, res) => {
        var Bucket = "production-optafeed";
        if(req.params.feed.endsWith(".xml")) {
            hl([{ Bucket, "Key": req.params.feed }])
                .flatMap(hl.wrapCallback(s3.getObject.bind(s3)))
                .stopOnError(() => {
                    res.send(404, key + " does not exist");
                })
                .each(result => {
                    res.send(result.Body.toString());
                })
        } else {
            hl([{ Bucket, Prefix: req.params.feed }])
                .flatMap(hl.wrapCallback(s3.listObjects.bind(s3)))
                .pluck('Contents')
                .map(R.pluck('Key'))
                .pull((err, result) => {
                    if(!R.isNil(err)) {
                        res.send(500);
                    }
                    else if(R.isEmpty(result)) {
                        res.send(404, "No files with the prefix: '" + req.params.feed + "' exists.");
                    } else {
                        res.send(result.filter(R.test(/.xml/)))
                    }
                })
        }
    }
};

