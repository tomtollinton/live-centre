var request = require('request');
var hl = require('highland');
var R = require('ramda');
var argv = require('minimist')(process.argv.slice(2));
var parseXMLString = hl.wrapCallback(require('xml2js').parseXMLString);

argv.e = argv.e || "stable";
if(argv.e[0]==='p' && argv.e[4]==='p') argv.e = "production";

var eceHost = {
    stable: 'nat-dev-cms-stable.mirror.co.uk:8080',
    'reg-stable': 'reg-dev-cms-stable.birminghammail.co.uk:8080',
    vulcan: 'nat-dev-cms-vulcan.mirror.co.uk:8080',
    'reg-vulcan': 'reg-dev-cms-vulcan.birminghammail.co.uk:8080',
    production: 'cms2a.mirror.co.uk:8080',
    heinz: 'nat-dev-cms14-heinz.mirror.co.uk:8080'
}[argv.e]

var ecePass = {
    stable: 'WRer95hE',
    'reg-stable': 'admin',
    vulcan: 'WRer95hE',
    'reg-vulcan': 'admin',
    production: 'WRer95hE',
    heinz: 'WRer95hE'
}[argv.e]

var eceUser = {
    stable: 'mirror_admin',
    'reg-stable': 'birminghammail_admin',
    vulcan: 'mirror_admin',
    'reg-vulcan': 'birminghammail_admin',
    production: 'mirror_admin',
    heinz: 'mirror_admin'
}[argv.e]

var uname = argv.u || eceUser || 'mirror_admin';
var pword = argv.p || ecePass || 'WRer95hE';

var stateToXml = {
    published: '<app:control xmlns:vaext="http://www.vizrt.com/atom-ext"><app:draft>no</app:draft><vaext:state>published</vaext:state></app:control>',
    draft: '<app:control xmlns:vaext="http://www.vizrt.com/atom-ext"><app:draft>yes</app:draft><vaext:state>draft</vaext:state></app:control>'
}

function getUrl(id, liveEvent){
    return ['http://', uname, ':', pword, '@', eceHost, liveEvent ? '/live-center-editorial/event/' : '/webservice/escenic/content/',id, liveEvent ? '/entries?count=1000' : ''].join('')
}

module.exports.changeState = hl.wrapCallback(function(state, id, callback){
    var url = getUrl(id);
    request(url, function(err, res, body){
        if (err) return console.log('error reading article ' + id + ' - ' + err);
        body = body.replace(/<app:control[^>]*?>.*?<\/app:control[^>]*?>/,  stateToXml[state]);
        var options = {
            url: url,
            body: body,
            headers: {
                'If-Match': res.headers.etag,
                'Content-Type': 'application/atom+xml'
            }
        };
        request.put(options, function(err, res, body){
            if (err || res.statusCode !== 204){
                return callback(err || body);
            }
            callback(null, id);
        })
    });
});

function getEventEntries(id){
    return hl.wrapCallback(request)(getUrl(id, true))
    .pluck('body')
    .map(JSON.parse)
    .pluck('entries')
    .sequence()
    .filter(R.propEq('state','published'))
}
module.exports.getEventEntries = getEventEntries;

function getEntry(id){
    return hl.wrapCallback(request)(['http://', uname, ':', pword, '@', eceHost, '/live-center-editorial/entry/', id].join(''))
    .pluck('body')
    .map(JSON.parse)
}
module.exports.getEntry = getEntry;

var changeEntryState = hl.wrapCallback(function (state, entry, callback){
    var newBody = R.assoc('state', state, entry);
    var options = {
        url: entry.self,
        body: JSON.stringify(newBody),
        auth: {
            user: uname,
            pass: pword 
        },
        headers: {
            'If-Match': '"' + entry.eTag + '"',
            'Content-Type': 'application/json'
        }
    }
    request.put(options, function(err, res, body){
        if (err || res.statusCode !== 204){
            return callback(err || body);
        }
        request.get(R.pick(['url', 'auth'], options), function(err, res, body){
            if (err || res.statusCode !== 200){
                return callback(err || body);
            }
            callback(null, JSON.parse(body))
        })
    })
});
module.exports.changeEntryState = changeEntryState;

function getArticle(id){
    return hl(request(getUrl(id)))
    .reduce('', R.concat)
    .flatMap(parseXMLString)
    .pluck('entry')
};
module.exports.getArticle = getArticle;

function getRelations(entry){
    return hl([entry])
    .pluck('link')
    .sequence()
    .pluck('$')
    .filter(R.propEq('rel','related'))
}
module.exports.getRelations = getRelations;

module.exports.getRelIdsOfType = R.curry(function (type, id){
    return hl([id])
    .flatMap(getArticle)
    .flatMap(getRelations)
    .filter(R.propEq('metadata:group', type))
    .pluck('dcterms:identifier');
})