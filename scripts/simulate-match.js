/**
 * Created by DayoAdeyemi on 07/12/2015.
 */
var AWS = require ( 'aws-sdk' );
var S3 = new AWS.S3 ({
    // key only allows read access to s3
    "accessKeyId": "AKIAIM5MRBX2KZ25JEPQ",
    "secretAccessKey": "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",
    "region": "eu-west-1"
});
var kinesis = new AWS.Kinesis ({ region: 'eu-west-1' });
var hl = require ( 'highland' );
var R = require ( 'ramda' );
var moment = require('moment-timezone');
var path = require('path');
var describeResources = require ( './describe-resources.js' );

var prodConf = {
    bucket: 'staging-optafeed'
};

var delay = R.curry(function (_ms, x){
    var ms = _ms < 0 ? 0 : _ms;
    return hl(function (push){
        setTimeout(function(){
            push(null, x);
            push(null, hl.nil);
        }, ms)
    });
})

function recalculateTime(startTime, emulatedStartTime, timeMult, commentTime){
    return moment(moment(emulatedStartTime) + (moment.tz(commentTime, 'UTC') - moment(startTime))/timeMult).format('YYYY-MM-DD HH:mm:ss');
}

function updateOptaTimeStamps(startTime, emulatedStartTime, timeMult, xml){
    var xs = xml.split(/TimeStamp="([^"]*)"/);
    var i = 1;
    while(xs[i]){
        xs[i] = 'TimeStamp="'+recalculateTime(startTime, emulatedStartTime, timeMult, xs[i])+'"';
        i += 2;
    }
    xml = xs.join('');

    var m = /last_modified="(\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d)"/g;

    xs = xml.split(m);
    i = 1;
    while(xs[i]){
        xs[i] = 'last_modified="' + recalculateTime(startTime, emulatedStartTime, timeMult, xs[i])+'"';
        i += 2;
    }
    return xs.join('');
}

function listObjectsStreamGenerator( prefix ) {
    var nextMarker;
    return hl ( function ( push, next ) {
        S3.listObjects ( {
            Delimiter: ',',
            Marker: nextMarker,
            MaxKeys: 1000,
            Bucket: prodConf.bucket,
            Prefix: prefix
        }, function ( error, results ) {
            if ( error ) {
                push ( error );
                push ( null, hl.nil );
                return next ();
            }
            nextMarker = results.Marker;
            R.map(push.bind(this, null), results.Contents);
            if ( results.IsTruncated ) return next ();
            return push ( null, hl.nil );
        } );
    } );
};

var getObject = hl.wrapCallback ( function ( key, cb ) {
    S3.getObject ( {
        Bucket: prodConf.bucket,
        Key: key
    }, cb );
} );

/**
 *
 * @param fixtureId fixture to replay
 * @param competitionId competition id fixture is in
 * @param seasonId season id fixture is in
 * @param startTime timestamp game originally started
 * @param multiplier number of game seconds to skip
 * @param skip number of game seconds to skip
 * @param streamName name of stream to push to
 * @returns {Stream<null>}
 */
function simulateMatch(fixtureId, competitionId, seasonId, startTime, multiplier, skip, streamName){

    if (!startTime) throw new Error('startTime is required');

    var emulatedStartTime = moment.now();

    return hl.merge( R.map(function(feedType){
        return listObjectsStreamGenerator ( [ competitionId, seasonId, feedType, fixtureId ].join ( '-' ) )
    }, ['F3', 'F9', 'F13m']) )
        .pluck('Key')
        .filter(function(key){ return ~key.indexOf('.xml')})
        .map(function (key){
            var pubTime = R.split('-', key)[6];
            var timeDiff = pubTime - moment(startTime).unix() - skip;
            var waitTime =  timeDiff < 0 ? 0 : timeDiff/multiplier*1000;

            return hl([key])
                .tap(function(){
                    console.log('sending opta feed:', key, 'in', waitTime, 'ms');
                })
                .flatMap(R.partial(delay, [waitTime]))
                .flatMap( function ( Key ) {
                    return hl([Key, Key.replace('.xml', '.headers')])
                })
                .flatMap(getObject)
                .pluck('Body')
                .invoke('toString')
                .collect()
                .map(function(x){
                    return {
                        Data: updateOptaTimeStamps(startTime, emulatedStartTime, multiplier, x[0]),
                        PartitionKey: pubTime,
                        StreamName: streamName
                    }
                })
                .flatMap(hl.wrapCallback(kinesis.putRecord.bind(kinesis)))
                .tap(function(resp) {
                    var mins = Math.floor(timeDiff/60);
                    var secs = Math.floor(timeDiff)%60;
                    console.log("\n");
                    console.log('last feed: ', key);
                    console.log('original time:', new Date(pubTime*1000));
                    console.log('current time:', new Date());
                    console.log('time elapsed  - ', mins, ' mins and ', secs, ' seconds');
                })
        })
        .parallel(1000)
}


if (process.argv[1] === __filename){
    var args = require ( 'minimist' )( process.argv.slice(2) );
    var validate = function ( args ) {
        var usage = function () {
            console.log ( 'gameSequencer <options> <feedNumbers>' );
            console.log ( '  options:' );
            console.log ( '    -f|--fixtureId <fixtureId> (fixture to replay)' );
            console.log ( '    -c|--competitionId <competitionId> (competition id fixture is in)' );
            console.log ( '    -s|--seasonId <seasonId> (season id fixture is in)' );
            console.log ( '    -k|--skip <seconds> (number of game seconds to skip)' );
            console.log ( '    -m|--start-time <seconds> (timestamp game originally started)' );
            console.log ( '    -t|--timeMultiplier <factor> (play the fixture this much faster than real time)' );
            console.log ( '  streamName:' );
            console.log ( '    name of stream to push to' );
            process.exit ( 1 );
        };

        R.forEach ( function ( arg ) {
            if ( R.isNil ( arg ) ) {
                usage ();
            }
        }, [
            args.f || args.fixtureId,
        ] );
    };

    validate (args);

    var fixtureId = args.f || args.fixtureId;
    var competitionId = args.c || args.competitionId || 8;
    var seasonId = args.s || args.seasonId || 2014;
    var startTime = args.m || args['start-time'];
    var multiplier = ( args.t ? parseInt(args.t, 10) : null ) || 1;
    var skip = ( args.k ? parseInt(args.k, 10) : null ) || 0;

    describeResources(args._[0])
        .flatMap(resources =>
            simulateMatch(fixtureId, competitionId, seasonId, startTime, multiplier, skip, resources.OptaStream))
        .errors(console.error)
        .toArray(function(){
            console.log('done');
        })
} else {
    module.exports = simulateMatch;
}
