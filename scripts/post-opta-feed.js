/**
 * Created by DayoAdeyemi on 07/12/2015.
 */
var hl = require ( 'highland' );
var R = require ( 'ramda' );
var AWS = require( 'aws-sdk' );
var kinesis = new AWS.Kinesis({ region: 'eu-west-1' });
var s3 = new AWS.S3({
    // key only allows read access to s3
    "accessKeyId": "AKIAIM5MRBX2KZ25JEPQ",
    "secretAccessKey": "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",
    "region": "eu-west-1"
});
var util = require ( 'util' );
var describeResources = require ( './describe-resources.js' );
var Bucket = 'staging-optafeed';

    /**
 *
 * @param fixtureId fixtureId of match to clear
 * @param streamName name of stream to push to
 */
function postOptaFeed(Key, streamName){
    return hl([{ Bucket, Prefix: Key }])
    .flatMap(hl.wrapCallback(s3.listObjects.bind(s3)))
    .pluck('Contents')
    .flatMap(Contents => {
        var Keys = R.pluck('Key', Contents);
        if (Keys.length !== 2) return hl(Keys);
        return hl(Keys).last()
            .map(Key => ({ Bucket, Key }))
            .flatMap(hl.wrapCallback(s3.getObject.bind(s3)))
            .pluck('Body')
            .map(Data => ({
                Data,
                PartitionKey: (new Date()).valueOf().toString(),
                StreamName: streamName
            }))
            .flatMap(hl.wrapCallback(kinesis.putRecord.bind(kinesis)))
    })
}

if (process.argv[1] === __filename){
    var args = process.argv.slice(2);
    describeResources(args[0])
        .flatMap(resources => postOptaFeed(args[1], resources.OptaStream))
	    .errors(e => console.log(e.stack))
        .each(console.log);
} else {
    module.exports = postOptaFeed;
}
