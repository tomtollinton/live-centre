/**
 * Created by DayoAdeyemi on 28/01/2016.
 */
var AWS = require ( 'aws-sdk' );
var kinesis = new AWS.Kinesis ({ region: 'eu-west-1' });
var hl = require ( 'highland' );
var R = require ( 'ramda' );
var util = require ( 'util' );
var clearMatchComments = require ( './clear-match-comments.js' );


function initializeFixture(psURL, fixtureId){
    var ps = require('presentation-service')(psURL);
    return ps.get('/v1/live-centre/opta/fixtures/'+fixtureId)
        .map(R.assoc('period', 'PreMatch'))
        .map(R.assoc('lastUpdated', 0))
        .map(R.assoc('period', 'PreMatch'))
        .map(R.omit(['stats']))
        .map(R.assocPath(['homeTeam','score'], 0))
        .map(R.assocPath(['homeTeam','penaltyShots'], []))
        .map(R.assocPath(['homeTeam','goalsScored'], []))
        .map(R.assocPath(['awayTeam','score'], 0))
        .map(R.assocPath(['awayTeam','penaltyShots'], []))
        .map(R.assocPath(['awayTeam','goalsScored'], []))
        .flatMap(fixture => ps.put('/v1/live-centre/opta/fixtures/'+fixtureId, fixture))
}
function clearStatSnapshots(psURL, fixtureId){
    var ps = require('presentation-service')(psURL);
    return hl(R.range(0,10))
    .map(period => '/v1/live-centre/opta/statSnapshots/'+fixtureId+'/'+period)
    .map(uri => hl.merge([
        ps.del(uri),
        ps.del(uri.replace('/live-centre', '')),
        ps.rem('/v1/live-centre/opta-commentary/'+fixtureId, '${'+uri+';null}'),
        ps.rem('/v1/live-centre/opta-commentary/'+fixtureId, '${'+uri.replace('/live-centre', '')+';null}')
    ]))
    .merge()
}
/**
 *
 * @param fixtureId fixtureId of match to clear
 * @param streamName name of stream to push to
 */
function clearMatch(fixtureId, psURL, streamName){
    return hl.merge([
        clearMatchComments(fixtureId, streamName),
        initializeFixture(psURL, fixtureId),
        clearStatSnapshots(psURL, fixtureId)
    ]);
}


if (process.argv[1] === __filename){
    var describeResources = require ( './describe-resources.js' );
    var args = process.argv.slice(2);
    if (!args[1]) {
        console.log('usage: `node clear-match <environment> <fixtureId>`');
        process.exit(1);
    }
    describeResources(args[0])
        .tap(console.log)
        .flatMap(resources => clearMatch(args[1], resources.PresentationServiceEndpointURL, resources.OptaStream))
        .errors(e => console.log(e.stack))
        .each(console.log);
} else {
    module.exports = clearMatch;
}
