"use strict";
var assert = require("chai").assert;
var fs = require("fs");
var f13m = fs.readFileSync("test/f13m.xml", "utf8");
var f9 = fs.readFileSync("test/f9.xml", "utf8");
var hl = require("highland");
var parseString = hl.wrapCallback(require('xml2js').parseString);
var rewire = require('rewire');
var simMatch = rewire("../simulate-match");
var recalculateTime = simMatch.__get__('recalculateTime');
var updateOptaTimeStamps = simMatch.__get__('updateOptaTimeStamps');

describe("recalculateTime", () => {

    it.skip("does something", () => {
        assert.equal(recalculateTime('2014-09-13 15:00:00', '2016-01-01 16:00:00', 2, '2014-09-13 15:53:36'), '2016-01-01 16:56:48');
    });

    it.skip("does something else", () => {
        assert.equal(recalculateTime('2014-09-13 15:00:00', '2016-01-01 16:00:00', 2, '2014-09-13 13:18:05'), '2016-01-01 15:39:02');
    });


    it("does something else", () => {
        assert.equal(recalculateTime('20141118T200000+0000', '2016-01-01 16:00:00', 2, "20141118T202428+0000"), "2016-01-01 16:12:14");
    });
});

describe("our test", () => {
    it("does something for now", done => {

        const timeMult = 2;
        const emulatedStartTime = '2016-01-01 16:00:00';
        const startTime = '2014-09-13 15:00:00';
        const calculatedXML = updateOptaTimeStamps(startTime, emulatedStartTime, timeMult, f13m);

        hl.sequence([parseString(f13m), parseString(calculatedXML)])
        .collect()
        .map(_ => {
            let original = _[0];
            let calculated = _[1];
            let originalTimes = original.Commentary.message.map(_ => _.$.last_modified);
            let calculatedTimes = calculated.Commentary.message.map(_ => _.$.last_modified);
            for (let i=0; i<originalTimes.length; i++){
                assert.equal(calculatedTimes[i], recalculateTime(startTime, emulatedStartTime, timeMult, originalTimes[i]));
            }
        })
        .pull(done);

    });
    it("does something for now", done => {

        const timeMult = 2;
        const emulatedStartTime = '2016-01-01 16:00:00';
        const startTime = '20141118T200000+0000';
        const calculatedXML = updateOptaTimeStamps(startTime, emulatedStartTime, timeMult, f9);

        hl.sequence([parseString(f9), parseString(calculatedXML)])
            .collect()
            .map(_ => {
                let original = _[0];
                let calculated = _[1];
                let originalTime = original.SoccerFeed.$.TimeStamp;
                let calculatedTime = calculated.SoccerFeed.$.TimeStamp;
                assert.equal(calculatedTime, recalculateTime(startTime, emulatedStartTime, timeMult, originalTime));
            })
            .pull(done);

    });
});