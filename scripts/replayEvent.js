var argv = require('minimist')(process.argv.slice(2));
var hl = require('highland');
var R = require('ramda');
var changeEntryState = require('./escUtils.js').changeEntryState;
var getEventEntries = require('./escUtils.js').getEventEntries;
var moment = require('moment');

var delay = R.curry(function (_ms, x){
	var ms = _ms < 0 ? 0 : _ms;
	return hl(function (push, next){
		setTimeout(function(){
			push(null, x);
			push(null, hl.nil);
		}, ms)
	});
})

var startTime = argv['start-time']
var multiplier = ( argv.t ? parseInt(argv.t, 10) : null ) || 1
var skip = ( argv.k ? parseInt(argv.k, 10) : null ) || 0

if (!startTime){
	console.error('--start-time <timestamp> is required')
	process.exit(1);
}

hl(argv._)
.flatMap(getEventEntries)
.map(function(entry){
	var timeDiff = moment(entry.creationDate).unix()-moment(startTime).unix() - skip;
	console.log(timeDiff)
	var waitTime =  timeDiff < 0 ? 0 : timeDiff/multiplier*1000;
	var id = R.head(entry.self.split('/').reverse())
	
	return hl([entry])
	.flatMap(R.partial(changeEntryState, ['deleted']))
	.tap(function(){
		console.log('publishing', entry.self, 'in', waitTime, 'ms');
	})
	.flatMap(R.partial(delay, [waitTime]))
	.flatMap(R.partial(changeEntryState, ['published']))
	.tap(function(){
	    var mins = Math.floor(timeDiff/60)%60;
	    var secs = Math.floor(timeDiff)%60;
	    console.log('\n');
	    console.log('last Entry: ', id);
	    console.log('original time:', moment(entry.creationDate).format())
	    console.log('current time:', new Date())
	    console.log('time elapsed  - ', mins, ' mins and ', secs, ' seconds');
	})
})
.merge()
.errors(function(e){ console.log(e) })
.toArray(function(entries){
	console.log('done');
})