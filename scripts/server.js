"use strict";

var restify = require('restify');
var server = restify.createServer();
var OptaController = require("./OptaController");

var functions = {
    "describe-resources": {
        "args": [
            "environment"
        ],
        "output": true
    },
    "clear-match-comments": {
        "args": [
            "fixtureId",
            "streamName"
        ],
        "output": true
    },
    "clear-match": {
        "args": [
            "fixtureId",
            "psURL",
            "streamName"
        ],
        "output": true
    },
    "post-opta-feed": {
        "args": [
            "S3Key",
            "streamName"
        ],
        "output": true
    },
    "simulate-match": {
        "args": [
            "fixtureId",
            "competitionId",
            "seasonId",
            "startTime",
            "multiplier",
            "skip",
            "streamName"
        ]
    }
};
server.use(restify.bodyParser());

for (let methodName in functions){
    let args = functions[methodName].args,
        output = functions[methodName].output;
    const runScript = (req, res) => {
        var isFirst = true;
        try{ if (req.body && req.body.length > 0 ) req.params = JSON.parse(req.body) }
        catch (e) {}
        require('./'+methodName +'.js')
            .apply(null, args.map(arg => req.params[arg]))
            .map(result => {
                if (output)  {
                    if (isFirst) res.write('[');
                    else res.write(',');
                    res.write(JSON.stringify(result));
                    isFirst = false;
                }
            })
            .done(()=> {
                if (output) {
                    res.end(']');
                }
            });
        if (!output) res.send('running')
    };
    server.post('/'+ methodName, runScript);
    server.get('/'+ methodName +'/' + args.map(param => ':'+param).join('/'), runScript);
}

server.get("/opta/:feed", OptaController.getFeed);

server.get('/', function (req, res) {
    res.send(functions);
});

server.on('uncaughtException', function (req, res, route, err) {
    console.error(err.stack);
    res.write(err.stack);
    res.end();
});

server.listen(process.env.PORT || 8080, function () {
    console.log('%s listening at %s', server.name, server.url);
});

