var hl = require ( 'highland' );
var AWS = require ( 'aws-sdk' );
var cloudformation = new AWS.CloudFormation({ region: 'eu-west-1' });
var cfn = hl.streamifyAll(cloudformation, cloudformation);

function describeResources(env){
    if(env === 'prod') {
        return hl([{
            PresentationAPIEndpoint: 'livecentreps-prod.eu-west-1.elasticbeanstalk.com',
            PresentationUpdateAPIEndpoint: 'livecentrepsu-prod.eu-west-1.elasticbeanstalk.com'
        }]);
    }
    return cfn.describeStacksStream({ StackName: 'LiveCentre-' + env })
        .map(resp =>
            resp.Stacks[0].Outputs.reduce(
                (obj, output) => (obj[output.OutputKey] = output.OutputValue) && obj, {}));
}


if (process.argv[1] === __filename){
    var args = process.argv.slice(2);
    describeResources(args[0])
        .errors(e => console.error(e.stack))
        .each(console.log);
} else {
    module.exports = describeResources;
}
