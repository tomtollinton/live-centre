/**
 * Created by DayoAdeyemi on 07/12/2015.
 */
var AWS = require ( 'aws-sdk' );
var kinesis = new AWS.Kinesis ({ region: 'eu-west-1' });
var hl = require ( 'highland' );
var util = require ( 'util' );
var describeResources = require ( './describe-resources.js' );

/**
 *
 * @param fixtureId fixtureId of match to clear
 * @param streamName name of stream to push to
 */
function clearMatchComentary(fixtureId, streamName){
    var params = {
        Data: util.format('<Commentary game_id="%d"> </Commentary>', fixtureId),
        PartitionKey: (new Date()).valueOf().toString(),
        StreamName: streamName
    }
    return hl.wrapCallback(kinesis.putRecord.bind(kinesis))(params);
}


if (process.argv[1] === __filename){
    var args = process.argv.slice(2);
    if (!args[1]) {
        console.log('usage: `node clear-match-comments <environment> <fixtureId>`');
        process.exit(1);
    }
    describeResources(args[0])
        .flatMap(resources => clearMatchComentary(args[1], resources.OptaStream))
	    .errors(e => console.log(e.stack))
        .each(console.log);
} else {
    module.exports = clearMatchComentary;
}
