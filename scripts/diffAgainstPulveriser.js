'use strict';
const argv = require('minimist')(process.argv.slice(2));
const assert = require('chai').assert;
const hl = require('highland');
const R = require('ramda');
const request = require('request');
const diff = require('difflet')({ indent : 2 });
const tz = require('timezone/loaded');
const unpair = require('tm-uniqid').undo;

const pulveriserEnv = argv['pulveriser-env'];
const chronosEnv = argv['chronos-env'];
const platform = argv['platform'];
const articleId = argv['article-id'];

if(R.isNil(articleId)) {
    console.log('An article ID must be provided with the --article-id parameter.');
    process.exit(1);
}

const pulveriserUrls = {
    vulcan: "http://vulcan-api.elasticbeanstalk.com",
    stable: "http://stable-api.elasticbeanstalk.com",
    prod: "http://pulveriser-api-prod.tm-aws.com"
};

const chronosUrls = {
    vulcan: "http://livecentreps-vulcan.eu-west-1.elasticbeanstalk.com",
    stable: "http://livecentreps-stable.eu-west-1.elasticbeanstalk.com",
    prod: "http://livecentreps-prod.eu-west-1.elasticbeanstalk.com"
};

if(!R.contains(chronosEnv, R.keys(chronosUrls))) {
    console.log('--chronos-env must be one of ' + R.keys(chronosUrls).join(' | '));
    process.exit(1);
}

if(!R.contains(pulveriserEnv, R.keys(pulveriserUrls))) {
    console.log('--pulveriser-env must be one of ' + R.keys(pulveriserUrls).join(' | '));
    process.exit(1);
}

if(!R.contains(platform, ['nationals', 'regionals'])) {
    console.log("--platform parameter must be either 'regionals' or 'nationals'.");
    process.exit(1);
}

// createPulveriserLiveEventUrl :: Integer -> String
const createPulveriserLiveEventUrl = id => [pulveriserUrls[pulveriserEnv], 'liveEventEntries', platform === 'nationals' ? 'mirror' : 'men', id].join('/');

// createChronosLiveEventUrl :: Integer -> String
const createChronosLiveEventUrl = id => [chronosUrls[chronosEnv], 'v1', 'live-centre', platform + '-live', id].join('/');

// getJson :: String -> Stream Object
const getJson = url => {
   return hl(request(url))
       .invoke('toString', ['utf8'])
       .reduce1(R.concat)
       .map(JSON.parse)
};

// convertToChronos :: String -> Integer -> Object -> Object
const convertToChronos = R.curry((platform, id, entry) => {
    const postTime = entry.unixTime;
    return R.reject(R.isNil, {
        id: R.head(unpair(entry.id)),
        author: R.isEmpty(entry.authorName) ? void 0 : entry.authorName,
        title: entry.title,
        postTime: postTime,
        createdDate: tz(postTime * 1000, '%Y-%m-%dT%H:%M:%S%:z', 'Europe/London'),
        type: entry.entryStyle,
        hideTimestamp: entry.hideTimestamp,
        isSticky: entry.sticky,
        prefix: entry.prefix,
        event: [platform + '-live', id].join('/'),
        content: R.map(convertContentTypesToChronos, entry.contentTypes)
    })
});

// convertContentTypesToChronos :: Object -> Object
const convertContentTypesToChronos = contentType => {
    switch(contentType.type) {
        case 'paragraph':
            return {
                type: 'text',
                text: contentType.text
            };
        case 'blockquote':
            return {
                type: contentType.type,
                text: contentType.text
            };
        case 'image':
            return {
                url: contentType.src.replace(/http:\/\/i\d+/, 'http://www').toLowerCase(),
                type: contentType.type,
                caption: contentType.caption,
                altText: contentType.altText,
                id: parseInt(contentType.src.match(/(\d+)\.ece/)[1]),
                imageCredit: contentType.imageCredit
            };
        case 'videoGallery':
            return {
                type: 'escenic',
                url: contentType.url
            };
        case 'tweet':
            return {
                type: contentType.type,
                tweetId: contentType.tweetId
            };
        case 'instagram':
            return {
                type: contentType.type,
                url: contentType.imageUrl
            };
        case 'html':
            return {
                type: contentType.type,
                text: contentType.text
            };
        case 'photoGallery':
            return {
                type: 'escenic',
                url: contentType.url
            };
        default:
            return {
                type: 'escenic',
                url: contentType.url
            };
    }
};

// normaliseLiveEventContent :: Object -> Object
const normaliseLiveEventContent = item => {
    if (item.type === 'tweet') {
        return {
            type: item.type,
            tweetId: item.url.match(/\/(\d+)(?=[^\/]*$)/)[1]
        }
    } else if(item.type === 'html') {
        return R.omit(['title'], item);
    } else if(item.type === 'image') {
        return R.assoc('url', item.url.toLowerCase(), item);
    } else if(item.type === 'blockquote') {
        return R.omit(['cite'], item);
    } else {
        return item;
    }
};

// normaliseChronosJson :: Object -> Object
const normaliseChronosJson = R.pipe(
    R.omit(['lastModifiedTime']),
    R.evolve({
        content: R.compose(R.map(normaliseLiveEventContent), R.reject(R.isNil)),
        postTime: ms => {
            return Math.floor(ms / 1000);
        }
    }), entry => {
        entry.createdDate = tz(entry.postTime * 1000, '%Y-%m-%dT%H:%M:%S%:z', 'Europe/London');
        return entry;
    });

// pulveriserEntries :: Int -> Stream Object
const pulveriserEntries = getJson(createPulveriserLiveEventUrl(articleId))
    .map(R.path(['articles', 'content']))
    .map(R.reverse)
    .map(R.map(convertToChronos(platform, articleId)));

// chronosEntries :: Int -> Stream Object
const chronosEntries = getJson(createChronosLiveEventUrl(articleId))
    .map(R.map(normaliseChronosJson));

hl.zip(chronosEntries, pulveriserEntries)
    .map(lists => {
        const pulvIds = R.pluck('id', lists[0]);
        const chronosIds = R.pluck('id', lists[1]);
        const missingPulv = R.difference(chronosIds, pulvIds);
        const missingChronos = R.difference(pulvIds, chronosIds);
        const normalise = xs => R.compose(R.sortBy(R.prop('id')), R.concat(xs), R.map(id => R.objOf('id', id)));
        const normalisedPulv = normalise(lists[0])(missingPulv);
        const normalisedChronos = normalise(lists[1])(missingChronos);
        return [normalisedPulv, normalisedChronos];
    })
    .flatMap(lists => {
        const pulv = hl(lists[0]);
        const chronos = hl(lists[1]);
        return hl.zip(chronos, pulv)
    })
    .reject(R.apply(R.equals))
    .tap(pair => {
        console.log(diff.compare(pair[0], pair[1]))
    })
    .toArray(xs => {
        if(xs.length !== 0) {
            process.exit(1);
        }
        console.log('Contents match!');
        process.exit(0);
    });