#!/bin/bash
set -e

: <<'END'
export ENVIRONMENT_SUFFIX=vulcan
export AWS_ACCOUNT_SUFFIX=dev
export AWS_DEFAULT_REGION=eu-west-1
export AWS_REGION=eu-west-1
END

ansible-playbook infrastructure.yml
ansible-playbook update.yml