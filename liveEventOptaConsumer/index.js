"use strict";
var R = require('ramda');
var hl = require('highland');
var leagueTable = require('./leagueTable');
var getComments = require('./getComments');
var fixtures = require('./fixtures');
var feeds = require('./feeds');
var parseXml = hl.wrapCallback(require('xml2js').parseString);

var log = R.curry(function(seq, feedType, json) {
    console.log("Seq#", seq,"type:", feedType, "ID:", feeds.idFromFeed(feedType, json));
});

exports.handler = function (event, context) {
    hl(event.Records)
        .flatMap(function(record){
            var xml = new Buffer(record.kinesis.data, 'base64').toString('utf8');
            var feed = feeds.feedType(xml);
            var parse = parseXml(xml).tap(log(record.kinesis.sequenceNumber, feed));
            switch(feed) {
                case "F13m": return getComments(parse);
                case "F9": return fixtures.getSummary(parse);
                case "F3": return leagueTable.transformFeed(parse);
                case "F1": return fixtures.getFixtureList(parse);
                default: return hl([feed + " not supported"]);
            }
        })
        .errors(function (err) {
            console.error(err);
            console.error(err.stack);
        })
        .collect()
        .pull(context.done);
};