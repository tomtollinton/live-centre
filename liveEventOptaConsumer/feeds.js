'use strict';
var R = require('ramda');

function feedType(xml) {
    var isF13m = R.test(/<\/Commentary>\s*$/, xml);
    var isOther = xml.match(/Opta::Feed::XML::Soccer::(F\d\d?m?)/);

    if(isF13m) return "F13m";
    else if(!!isOther) return isOther[1];
    else return "Unidentified feed";
}

function idFromFeed(feed, json){
    switch (feed) {
        case "F13m":
            return R.path(["Commentary", "$", "game_id"], json);
        case "F9":
            return R.path(["SoccerFeed", "SoccerDocument", 0, "$", "uID"], json);
        case "F3":
        case "F1":
            var f3Meta = R.path(["SoccerFeed", "SoccerDocument", 0, "$"], json);
            return "COMP_ID " + f3Meta.competition_id + " Season: " + f3Meta.season_id;
    }
}

module.exports = {
    feedType: feedType,
    idFromFeed: idFromFeed
};