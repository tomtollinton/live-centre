"use strict";
const chai = require('chai');
const assert = chai.assert;
const rewire = require('rewire');
const fs = require('fs');
const R = require('ramda');
var moment = require('moment-timezone');
var parseString = require('xml2js').parseString;

describe('unit tests', () => {
    describe('utils.js',() => {
        const utils = require("../../utils");
        describe('getMinuteWithInjuryTime', () => {
            const getMinuteWithInjuryTime = utils.getMinuteWithInjuryTime;

            it('should get correct times outside of injury time', () => {
                R.range(0, 46).forEach((x) => {
                    assert.equal(x.toString(), getMinuteWithInjuryTime(x.toString(), '1'));
                });
                R.range(46, 91).forEach((x) => {
                    assert.equal(x.toString(), getMinuteWithInjuryTime(x.toString(), '2'));
                });
            });

            it('should correct the time in injury time', () => {
                R.range(46, 60).forEach((x) => {
                    assert.equal('45+' + (x - 45).toString(), getMinuteWithInjuryTime(x.toString(), '1'));
                });
                R.range(91, 110).forEach((x) => {
                    assert.equal('90+' + (x - 90).toString(), getMinuteWithInjuryTime(x.toString(), '2'));
                });
            });

            it('should return the empty string if the minute has a bad value', () => {
                assert.equal("", getMinuteWithInjuryTime(undefined, '1'));
                assert.equal("", getMinuteWithInjuryTime(null, '1'));
                assert.equal("", getMinuteWithInjuryTime('', '1'));
                assert.equal("", getMinuteWithInjuryTime({}, '1'));
            });
        });

        describe('getTimesStamp', () => {
            it("handles dates with times", () => {
                const gmtTime = "2015-06-20 13:30:00";
                const localTime = "2015-06-20 14:30:00";
                assert.equal(utils.timeStampFromLocal(localTime), utils.timeStampFromGmt(gmtTime))
            });

            it("handles dates with no times", () => {
                assert.equal(utils.timeStampFromLocal("2015-06-20"), utils.timeStampFromLocal("2015-06-20"));
            });

            it("handles dates that are full ISO1608", () => {
                assert.equal(utils.timeStampFromLocal("20150808T140006+0100"), "2015-08-08T14:00:06.000+01:00");
                // as the timezone is set they're the same
                assert.equal(utils.timeStampFromGmt("20150808T140006+0100"), "2015-08-08T14:00:06.000+01:00");
            });

            it("unix methods also work as expected", () => {
                const gmtTime = "2015-06-20 13:30:00";
                const localTime = "2015-06-20 14:30:00";
                assert.equal(utils.unixLocal(localTime), 1434807000000);
                assert.equal(utils.unixGmt(gmtTime), 1434807000000);
            })
        });
    });

    describe('getComments.js',() => {
        const getComments = rewire('../../getComments.js');
        describe('getContentTypeArray', () => {
            const getContentTypeArray = getComments.__get__('getContentTypeArray');
            it('should be empty for an empty message', () => {
                assert.equal(0, getContentTypeArray("            ").length);
            });

            it('should clean a paragraph of trailing spaces', () => {
                assert.deepEqual({ type: 'text', text: 'Test paragraph' },
                    getContentTypeArray('  Test paragraph  ')[0])
            });

            it('should remove empty paragraphs', () => {
                assert.deepEqual(
                    [
                        { type: 'text', text: 'Test1'},
                        { type: 'text', text: 'Test2'}
                    ],
                    getContentTypeArray('Test1\n   \nTest2'))
            });

            it('should split on windows line endings too', () => {
                assert.deepEqual([
                        { type: 'text', text: 'Test1'},
                        { type: 'text', text: 'Test2'}
                    ],
                    getContentTypeArray('Test1\r\nTest2'))
            })
        });

        describe('correctNewItemsPublishedTime', () => {
            var correctNewItemsPublishedTime = getComments.__get__('correctNewItemsPublishedTime');
            it('should leave alone a new item', () => {
                var item = {"id":123, "postTime": 12345, "matchTime":"14:00", "otherProperty":"Some other values"};
                var originalItem = R.clone(item);
                correctNewItemsPublishedTime([item], []);
                assert.deepEqual(originalItem, item);
            });

            it('should modify an updated item', () => {
                var newItem = {"id":123, "postTime": 12345, "matchTime":"14:00", "otherProperty":"Some new other values"};
                var oldItem = {"id":123, "postTime": 1200, "matchTime":"13:00", "otherProperty":"Some old other values"};
                correctNewItemsPublishedTime([newItem], [oldItem]);
                assert.deepEqual(R.merge(oldItem, {"otherProperty":"Some new other values"}), newItem);
            });
        });

        describe('convertOptaMessageToPresentationFormat', () => {
            const convertOptaMessageToPresentationFormat = getComments.__get__('convertOptaMessageToPresentationFormat');

            const input = {
                id: '2833181',
                comment: 'Tiote booked for a tackle on Cork.',
                last_modified: '2014-09-13 15:14:50',
                minute: '57',
                period: '2',
                second: '0',
                time: '57\'',
                type: 'pre kick off'
            };

            it('should kebab case opta type (CRON-331)', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).type, "opta-pre-kick off");
            });

            it('should convert lastModified to milliseconds', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).lastModifiedTime,
                    1410621290000);
            });

            it('should create createdat as ISO format', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).createdDate,
                    '2014-09-13T16:14:50.000+01:00');
            });

            it('should create createdAt in milliseconds', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).postTime,
                    1410621290000);
            });

            it('should rename minute to matchTime', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).matchTime,
                    '57');
            });

            it('should preserve id', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).id,
                    2833181);
            });

            it('should set hideTimestamp to false', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).hideTimestamp,
                    false);
            });

            it('should set isSticky to false', () => {
                assert.strictEqual(convertOptaMessageToPresentationFormat(input).isSticky,
                    false);
            });

        });
    });

    describe('feeds.js', () => {
        const feeds = rewire('../../feeds.js');
        const F13mFeed = fs.readFileSync(__dirname + '/../data/F13m.xml', 'utf8');
        const F9Feed = fs.readFileSync(__dirname + '/../data/F9-803162.xml', 'utf8');
        const F3Feed = fs.readFileSync(__dirname + '/../data/F3-2015-803326.xml', 'utf8');


        describe("feedType() should", () => {
            const feedType = feeds.__get__('feedType');
            it("get F13 type from xml", () => {
                assert.equal(feedType(F13mFeed), "F13m");
            });

            it("get F9 type from xml", () => {
                assert.equal(feedType(F9Feed), "F9");
            });

            it("get F3 type from xml", () => {
                assert.equal(feedType(F3Feed), "F3");
            });
        });

        describe("isFromFeed", () => {
            const idFromFeed = feeds.__get__('idFromFeed');
            it("get Id from F13m feed", done => {
                parseString(F13mFeed, function (err, result) {
                    assert.equal(idFromFeed("F13m", result), "803326");
                    done(err);
                });
            });

            it("get Id from F9 feed", done => {
                parseString(F9Feed, function (err, result) {
                    assert.equal(idFromFeed("F9", result), "f803162");
                    done(err);
                });
            });

            it("get comp_id and season from F3 feed", done => {
                parseString(F3Feed, function (err, result) {
                    assert.equal(idFromFeed("F3", result), "COMP_ID 8 Season: 2015");
                    done(err);
                });
            });
        });
    });

    describe('leagueTable.js', () => {
        const leagueTable = rewire("../../leagueTable");
        const utils = leagueTable.__get__('utils');
        const F3Feed = fs.readFileSync(__dirname + '/../data/F3-2015-803326.xml', 'utf8');

        describe("formatF3()", () => {
            const formatF3 = leagueTable.__get__('formatF3');
            it("formats the F3 feed to create the league tables", done => {

                parseString(F3Feed, function (err, result) {

                    const SoccerDocument = result.SoccerFeed.SoccerDocument[0];
                    const teamNameByReference = utils.getNameMapping(SoccerDocument.Team);
                    const competitionId = SoccerDocument.$.competition_id;
                    const seasonId = SoccerDocument.$.season_id;
                    const TeamStandings = SoccerDocument.Competition[0].TeamStandings;
                    const team1 = TeamStandings[0];
                    const lastUpdated = 1450724400000;

                    const test = formatF3(team1, teamNameByReference, competitionId, seasonId, lastUpdated);
                    assert.equal(test.length, 20);
                    assert.equal(test[0].lastUpdated, 1450724400000);
                    done();

                });

            });
        })
    });
});
