"use strict";
const chai = require('chai');
const assert = chai.assert;
const rewire = require('rewire');
const fs = require('fs');
const R = require('ramda');
const hl = require('highland');
const validate = require('jsonschema').validate;
const standingsSchema = require('../../../schemas/standings.json');
const fixturesSchema = require('../../../schemas/fixtures.json');
const entrySchema = require('../../../schemas/liveCenterSchema.json');

const Given = JSON.parse(fs.readFileSync(__dirname + '/../data/event.json'));

const index = rewire('../../index.js');
const getComments = rewire('../../getComments.js');
const utils = rewire("../../utils");
const leagueTable = rewire("../../leagueTable");
const fixtures = rewire("../../fixtures");



// mock pusher and presentation-service
var pusher = {};
var pusherTimeout = 0;
var ps = utils.ps;
utils.__set__("pusher", {
    trigger: (channel, event, data, socketId, cb) => {
        pusher[channel] = pusher[channel] || [];
        pusher[channel].push({ [event]: data });
        assert(R.is(String, channel));
        assert(R.is(String, event));
        assert(R.is(Object, data));
        setTimeout(cb, pusherTimeout);
    }
});

// mock league table functions
leagueTable.__set__('utils', utils);

// mock getComments functions
getComments.__set__('utils', utils);

// mock fixtures functions
fixtures.__set__('utils', utils);

// mock index functions
index.__set__('leagueTable', leagueTable);
index.__set__('getComments', getComments);
index.__set__('fixtures', fixtures);

const runHandler =  hl.wrapCallback((event, done) =>{
    index.handler(event, {
        done,
        fail: done,
        succeed: done.bind(null, null)
    })
});

function init(){
    pusher = {};
}

describe('index.js', () => {
    describe('handler faced with F1 event', () => {
        before(init);
        it('should upsert the fixtures data', (done) => {
            runHandler(Given.F1Event)
            .flatMap(() => ps.get('/v1/live-centre/opta-fixtures/6/2015'))
            .tap(fixtures => assert.deepEqual(validate(fixtures, fixturesSchema).errors, []))
            .pull(done)
        }).timeout(10000);
    });
    describe('handler faced with F9 event', () => {
        before(init);
        it('should add a statSnapshot post to the feed', (done) => {
            runHandler(Given.F9Event)
                .flatMap(() => ps.get('/v1/live-centre/opta-commentary/803361'))
                .map(entries => {
                    assert.ok(entries[0], 'there is a post added');
                    assert(R.is(String, entries[0].homeTeam));
                    assert(R.is(String, entries[0].awayTeam));
                    assert(R.is(Number, entries[0].homeTeamPossession));
                    assert(R.is(Number, entries[0].awayTeamPossession));
                    assert.deepEqual(validate(entries, entrySchema).errors, []);
                })
                .pull(done);
        });
        describe('statSnapshots stuff', () => {
            before(init);
            let period1post, period3post;
            it('should not create a statSnapshot post for the first 15 mins', (done) => {
                runHandler(Given['F9Event first 15 min'])
                    .flatMap(() => ps.get('/v1/live-centre/opta-commentary/786674;[]'))
                    .map(entries => assert.equal(entries.length, 0, 'there are no posts in the commentary feed'))
                    .pull(done);
            });
            it('should create a period 1 post after the first 15 mins', (done) => {
                runHandler(Given['F9Event 15 - 30 min'])
                    .flatMap(() => ps.get('/v1/live-centre/opta-commentary/786674;[]'))
                    .map(entries => {
                        assert.ok(entries[0], 'there is a post added');
                        assert.equal(entries[0].id, 1, 'it has id 1');
                        period1post = entries[0];
                    })
                    .pull(done);
            });
            it('should create a period 1 post after the first 15 mins', (done) => {
                runHandler(Given['F9Event 15 - 30 min (but later)'])
                    .flatMap(() => ps.get('/v1/live-centre/opta-commentary/786674;[]'))
                    .map(entries => {
                        assert.ok(entries[0], 'there is a post added');
                        assert.equal(entries.length, 1, 'there is at most one post');
                        assert.deepEqual(entries[0], period1post, 'it is the same as before');
                    })
                    .pull(done);
            });
            it('should create a period 2 post after 30 mins', (done) => {
                runHandler(Given['F9Event 30 - 45 min'])
                    .flatMap(() => ps.get('/v1/live-centre/opta-commentary/786674;[]'))
                    .map(entries => {
                        assert.ok(entries[1], 'there is a post added');
                        assert.equal(entries.length, 2, 'there are 2 posts');
                        assert.equal(entries[1].id, 2, 'it has id 2');
                    })
                    .collect().pull(done);
            });
            it('should create a half time snapshot', (done) => {
                runHandler(Given['F9Event 45 - HalfTime'])
                    .flatMap(() => ps.get('/v1/live-centre/opta-commentary/786674;[]'))
                    .map(entries => {
                        assert.ok(entries[2], 'there is a post added');
                        assert.equal(entries.length, 3, 'there are 3 posts');
                        assert.equal(entries[2].id, 3, 'it has id 3');
                        period3post = entries[2];
                    })
                    .collect().pull(done);
            });
            it('shouldn\'t create a post in first 15 mins after half time', (done) => {
                runHandler(Given['F9Event SecondHalf - 59'])
                    .flatMap(() => ps.get('/v1/live-centre/opta-commentary/786674;[]'))
                    .map(entries => {
                        assert.deepEqual(entries[2], period3post, 'it is the same as before');
                    })
                    .pull(done);
            });
            it('should create a period 4 update', (done) => {
                runHandler(Given['F9Event 60 - 75'])
                    .flatMap(() => ps.get('/v1/live-centre/opta-commentary/786674;[]'))
                    .map(entries => {
                        assert.ok(entries[3], 'there is a post added');
                        assert.equal(entries.length, 4, 'there are 4 posts');
                        assert.equal(entries[3].id, 4, 'it has id 4');
                        period3post = entries[3];
                    })
                    .collect().pull(done);
            });
        });
        it('should ccreate a snapshot in extra time', (done) => {
            runHandler(Given['F9Event after 120min game'])
                .flatMap(() => ps.get('/v1/live-centre/opta-commentary/127081;[]'))
                .map(entries => {
                    assert.ok(entries[0], 'there is a post added');
                    assert.equal(entries.length, 1, 'there is 1 post');
                    assert.equal(entries[0].id, 8, 'it has id 5');
                })
                .collect().pull(done);
        });
        it('should upsert the fixtures data', (done) => {
            runHandler(Given.F9Event)
                .flatMap(() => ps.get('/v1/live-centre/opta/fixtures/803361'))
                .map(fixtures => assert.deepEqual(validate([fixtures], fixturesSchema).errors, []))
                .pull(done)
        });
        it('should upsert the fixtures data for a grouped fixture', (done) => {
            runHandler(Given.F9EventGrouped)
                .flatMap(() => ps.get('/v1/live-centre/opta/fixtures/832999'))
                .map(fixture => assert.deepEqual(validate([fixture], fixturesSchema).errors, []))
                .pull(done)
        });
        it('should upsert the fixtures data for a fixture with a penalty shootout', (done) => {
            runHandler(Given.F9EventPenaltyShots)
                .flatMap(() => ps.get('/v1/live-centre/opta/fixtures/831285'))
                .map(fixture => assert.deepEqual(validate([fixture], fixturesSchema).errors, []))
                .pull(done);
        });
        it('should upsert the fixtures data for a grouped fixture', (done) => {
            runHandler(Given.F9EventGrouped)
                .flatMap(() => ps.get('/v1/live-centre/opta/fixtures/832999'))
                .map(fixture => assert.deepEqual(validate([fixture], fixturesSchema).errors, []))
                .pull(done);
        });
        it('should upsert the fixtures data for a fixture with a penalty shootout', (done) => {
            runHandler(Given.F9EventPenaltyShots)
                .flatMap(() => ps.get('/v1/live-centre/opta/fixtures/831285'))
                .map(fixture => assert.deepEqual(validate([fixture], fixturesSchema).errors, []))
                .pull(done);
        });
    });
    describe('handler faced with F3 event should create opta league table data', function() {
        before(function(){
            init();
            pusherTimeout = 50;
        });
        after(function(){
            pusherTimeout = 0;
        });

        it('standard F3', (done) => {
            runHandler(Given.F3Event)
                .tap(() => assert.equal(20, pusher['opta-standings.8.2015'].length))
                .flatMap(() => ps.get('/v1/live-centre/opta-standings/8/2015'))
                .tap(standings => assert.deepEqual(validate(standings, standingsSchema).errors, []))
                .pull(done);
        });

        it('F3 grouped', function(done){
            runHandler(Given.F3EventGrouped)
                .tap(() => assert.equal(53, pusher['opta-standings.235.112016'].length))
                .flatMap(() => ps.get('/v1/live-centre/opta-standings/235/112016'))
                .tap(standings => assert.deepEqual(validate(standings, standingsSchema).errors, []))
                .pull(done);
        });
    });
    describe('handler faced with mutiple entries', function() {
        before(function(){
            init()
            pusherTimeout = 50;
        });
        after(function(){
            pusherTimeout = 0;
        });
        it('sends the right number of pusher calls', (done) => {
            runHandler(Given.multipleEntriesEvent)
                .tap(() => assert.equal(67, pusher['opta-commentary.803326'].length))
                .flatMap(() => ps.get('/v1/live-centre/opta-commentary/803326'))
                .tap(entries => assert.equal(67, entries.length))
                .tap(entries => assert.deepEqual(validate(entries, entrySchema).errors, []))
                .pull(done);
        });

        it('doesn\'t update pusher unnecessarily', (done) => {
            runHandler(Given.multipleEntriesEvent)
                .tap(() => assert.equal(67, pusher['opta-commentary.803326'].length))
                .flatMap(() => ps.get('/v1/live-centre/opta-commentary/803326'))
                .tap(entries => assert.equal(67, entries.length))
                .tap(entries => assert.deepEqual(validate(entries, entrySchema).errors, []))
                .pull(done);
        });
    });
    describe('event with one entry', () => {
        before(init);
        it('sends though the event to the presentation service and pusher', (done) => {
            runHandler(Given.oneEntryEvent)
                .tap(() => assert.equal(1, pusher['opta-commentary.803327'].length))
                .tap(() => assert.ok(pusher['opta-commentary.803327'][0].update))
                .flatMap(() => ps.get(pusher['opta-commentary.803327'][0].update.self))
                .tap(entry =>  {
                    assert.equal(entry.id, 8033260000067);
                    assert.equal(entry.type, 'opta-comment');
                    assert.equal(entry.lastModifiedTime, 1450625022000);
                    assert.equal(entry.postTime, 1450625022000);
                    assert.equal(entry.createdDate, '2015-12-20T15:23:42.000+00:00');
                    assert.equal(entry.isSticky, false);
                    assert.equal(entry.hideTimestamp, false);
                    assert.equal(entry.matchTime, '0');
                    assert.deepEqual(entry.content, [ { type: 'text',
                            text: "That's all from Vicarage Road, thanks for joining us today. Good bye!" } ]);
                    return ps.get('/v1/live-centre/opta-commentary/803327')
                        .tap(entries => assert.equal(1, entries.length))
                        .tap(entries => assert.equal(entry, entries[0]))
                        .tap(entries => assert.deepEqual(validate(entries, entrySchema).errors, []))
                })
                .pull(done);
        });

        it('sends though updates to the presentation service and pusher', (done) => {
            runHandler(Given.oneEntryEventUpdated)
                .tap(() => assert.equal(2, pusher['opta-commentary.803327'].length))
                .tap(() => assert.ok(pusher['opta-commentary.803327'][1].update))
                .flatMap(() => ps.get(pusher['opta-commentary.803327'][1].update.self))
                .tap(entry =>  {
                    assert.equal(entry.id, 8033260000067);
                    assert.equal(entry.type, 'opta-comment');
                    assert.equal(entry.lastModifiedTime, 1450625025000);
                    assert.equal(entry.postTime, 1450625022000);
                    assert.equal(entry.createdDate, '2015-12-20T15:23:45.000+00:00');
                    assert.equal(entry.isSticky, false);
                    assert.equal(entry.hideTimestamp, false);
                    assert.equal(entry.matchTime, '0');
                    assert.deepEqual(entry.content, [ { type: 'text',
                        text: "That's all from Vicarage Road, thanks for joining us today. Good bye! Updated" } ]);
                    return ps.get('/v1/live-centre/opta-commentary/803327')
                        .tap(entries => assert.equal(1, entries.length))
                        .tap(entries => assert.equal(entry, entries[0]))
                        .tap(entries => assert.deepEqual(validate(entries, entrySchema).errors, []))
                })
                .pull(done);
        });

        it('deletes the entry when it no longer appears on the feed', (done) => {
            runHandler(Given.noEntriesEvent)
                .tap(() => assert.equal(pusher['opta-commentary.803327'].length, 3))
                .tap(() => assert.ok(pusher['opta-commentary.803327'][2].delete))
                .flatMap(() => ps.get(pusher['opta-commentary.803327'][2].delete.self + ';null'))
                .tap(entry => assert.notOk(entry))
                .flatMap(() => ps.get('/v1/live-centre/opta-commentary/803327'))
                .pull((err) => {
                    assert.ok(err);
                    assert.strictEqual(err.type, 'KeyNotFound');
                    assert.strictEqual(err.message, '/v1/live-centre/opta-commentary/803327 not available');
                    done();
                });
        });
    });
});
