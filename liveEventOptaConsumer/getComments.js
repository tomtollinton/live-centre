"use strict";
const R = require('ramda');
const hl = require('highland');
var utils = require("./utils");

/**
 * Modifies each new item that has a matching id to a currentPresentationItems, by updating it's postTime and
 * matchTime to match the current presentation item's.
 */
function correctNewItemsPublishedTime(newItems, currentPresentationItems) {
    R.map(function (matchingNewItem) {
        var matchingOldItem = R.find(R.propEq('id', matchingNewItem.id), currentPresentationItems);
        matchingNewItem.postTime = matchingOldItem.postTime;
        matchingNewItem.matchTime = matchingOldItem.matchTime;
    }, R.intersectionWith(R.eqProps('id'), newItems, currentPresentationItems));
}

function upsertItem(fixtureId, item) {
    var uri = '/v1/live-centre/opta/comments/' + item.id;
    return hl.sequence([
        utils.ps.put(uri, JSON.stringify(item)),
        utils.ps.add('/v1/live-centre/opta-commentary/' + fixtureId, item.postTime, '${' + uri + ';null}'),
        utils.tickleClient('opta-commentary.' + fixtureId, 'update', {
            self: uri
        })
    ]);
}

function deleteItem(fixtureId, item) {
    var uri = '/v1/live-centre/opta/comments/' + item.id;
    return hl.sequence([
        utils.ps.del(uri),
        utils.ps.rem('/v1/live-centre/opta-commentary/' + fixtureId, '${' + uri + ';null}'),
        utils.tickleClient('opta-commentary.' + fixtureId, 'delete', {
            self: uri
        })
    ]);
}

function updatePresentation(fixtureId, newItems, currPres) {
    return hl.merge(R.concat(
        R.differenceWith(R.eqBy(R.pick(["id", "lastModifiedTime"])), newItems, currPres).map(R.partial(upsertItem, [fixtureId])),
        R.differenceWith(R.eqProps("id"), currPres, newItems).map(R.partial(deleteItem, [fixtureId]))
    ));
}

function getContentTypeArray(comment) {
    return comment
        .replace(/\r/, '')
        .split(/\n/g)
        .map(function (text) {
            return text.trim();
        })
        .filter(function (elem) {
            return !R.isEmpty(elem);
        })
        .map(function (text) {
            return {
                type: 'text',
                text: text
            };
        });
}

function convertOptaMessageToPresentationFormat(message) {
    var lastModifiedDate = utils.unixGmt(message.last_modified),
        incidentType = message.type.replace(" ", "-");
    return {
        id: parseInt(message.id),
        type: 'opta-' + incidentType,
        lastModifiedTime: lastModifiedDate,
        postTime: lastModifiedDate,
        createdDate: utils.timeStampFromGmt(message.last_modified),
        hideTimestamp: false,
        isSticky: false,
        matchTime: utils.getMinuteWithInjuryTime(message.minute, message.period),
        content: getContentTypeArray(message.comment)
    };
}

module.exports = function getComments(xml) {
    return xml
        .pluck('Commentary')
        .flatMap(function (Commentary) {
            var newItems, fixtureId = Commentary.$.game_id;
            if (Commentary.message !== void 0) {
                newItems = R.pluck('$', Commentary.message).map(convertOptaMessageToPresentationFormat);
            } else {
                newItems = [];
            }
            return utils.ps.get('/v1/live-centre/opta-commentary/' + fixtureId + ';[]')
                .tap(R.partial(correctNewItemsPublishedTime, [newItems]))
                .flatMap(R.partial(updatePresentation, [fixtureId, newItems]));
        });
};