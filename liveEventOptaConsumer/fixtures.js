'use strict';
var R = require('ramda');
var hl = require('highland');
var utils = require("./utils");

var statType = {
    possession_percentage: '% Possession',
    total_scoring_att: 'Total shots',
    ontarget_scoring_att: 'Shots on target',
    fk_foul_lost: 'Fouls',
    total_yel_card: 'Yellow cards',
    total_red_card: 'Red cards'
};

function getNameMapping(Teams){
    return R.reduce(function(memo, Team){
        if(Team.Player){
            Team.Player.forEach(function(Player){
                memo[Player.$.uID] = Player.PersonName[0].Known ?
                    Player.PersonName[0].Known[0] :
                Player.PersonName[0].First[0] + ' ' + Player.PersonName[0].Last[0];
            });
        }
        memo[Team.$.uID] = Team.Name[0];
        return memo;
    }, {}, Teams);
}

module.exports = {
    getSummary: function (xml) {
    return xml
        .pluck('SoccerFeed')
        .flatMap(function (SoccerFeed) {
            var nameMap = getNameMapping(SoccerFeed.SoccerDocument[0].Team);
            var timestamp = utils.unixLocal(SoccerFeed.$.TimeStamp);
            var fixtureId = parseInt(SoccerFeed.SoccerDocument[0].$.uID.replace('f',''));
            var startDate =  utils.unixLocal(SoccerFeed.SoccerDocument[0].MatchData[0].MatchInfo[0].Date[0]);
            var competitionId = parseInt(SoccerFeed.SoccerDocument[0].Competition[0].$.uID.replace('c',''));
            var seasonId = parseInt(R.find(R.pathEq(['$','Type'],'season_id'), SoccerFeed.SoccerDocument[0].Competition[0].Stat)._.replace('c',''));
            var refereeNameData = SoccerFeed.SoccerDocument[0].MatchData[0].MatchOfficial[0].OfficialName[0];
            var period = SoccerFeed.SoccerDocument[0].MatchData[0].MatchInfo[0].$.Period;
            var fixtureData = {
                "id": fixtureId,
                "competitionId": competitionId,
                "seasonId": seasonId,
                "url": "${" + ['/v1/live-centre/opta/fixtures', fixtureId, 'article'].join('/') + ",url;}",
                "lastUpdated": timestamp,
                "startDate":  startDate,
                "period": period,
                "finished": SoccerFeed.SoccerDocument[0].$.Type === 'Result',
                "location": R.path(['Venue', 0, 'Name', 0], SoccerFeed.SoccerDocument[0]),
                "attendance": parseInt(R.path(['Attendance', 0], SoccerFeed.SoccerDocument[0].MatchData[0].MatchInfo[0])) || void 0,
                "referee": refereeNameData.First[0] + ' ' + refereeNameData.Last[0],
                "groupLabel": R.path(['Round',0,'Pool', 0], SoccerFeed.SoccerDocument[0].Competition[0])
            };
            return hl(SoccerFeed.SoccerDocument[0].MatchData[0].TeamData)
                .map(function(TeamData){
                    var side = TeamData.$.Side.toLowerCase();
                    var PenaltyShots = R.path(['ShootOut',0,'PenaltyShot'], TeamData) || [];
                    var obj = { stats: {} };
                    obj[side+'Team'] = {
                        id: parseInt(TeamData.$.TeamRef.replace('t','')),
                        score: parseInt(TeamData.$.Score),
                        name: nameMap[TeamData.$.TeamRef],
                        penaltyShots: PenaltyShots.map(function(PenaltyShot){
                            return {
                                eventTime: utils.timeStampFromLocal(PenaltyShot.$.TimesStamp),
                                playerName: nameMap[PenaltyShot.$.PlayerRef],
                                scored: PenaltyShot.$.Outcome === 'scored'
                            };
                        }),
                        goalsScored: TeamData.Goal ? TeamData.Goal.map(function(Goal){
                            return {
                                name: nameMap[Goal.$.PlayerRef],
                                typeOfGoal: Goal.$.Type.toLowerCase(),
                                minuteOfGoal: Goal.$.Time
                            };
                        }) : []
                    };
                    return TeamData.Stat.reduce(function(memo, Stat){
                        var $temp = {};
                        $temp[side] = Stat._;
                        if(statType[Stat.$.Type]) memo.stats[statType[Stat.$.Type]] = $temp;
                        return memo;
                    }, obj);
                })
                .reduce(fixtureData, utils.deepMerge)
                .map(function(content) {
                    content.stats = R.reduce(function(memo, pair) {
                        memo.push({
                            title: pair[0],
                            values: pair[1]
                        });
                        return memo;
                    }, [], R.toPairs(content.stats));

                    return content;
                })
                .flatMap(function(item){
                    var matchTime = R.prop('_', R.find(R.pathEq(['$','Type'], 'match_time'), SoccerFeed.SoccerDocument[0].MatchData[0].Stat));
                    var id = Math.floor(matchTime/15);
                    if (id === 0)return [item];
                    var statSnapshotURI= ['/v1/live-centre/opta/statSnapshots', fixtureId, id].join('/');
                    return utils.ps.get(statSnapshotURI+';null')
                        .flatMap(function(statSnapshot){
                            if (statSnapshot === null){
                                statSnapshot = {
                                    id: id,
                                    type: "statistics",
                                    hideTimestamp: false,
                                    isSticky: false,
                                    postTime: timestamp,
                                    createdDate: utils.timeStampFromLocal(timestamp),
                                    lastModifiedTime: timestamp,
                                    content:[],
                                    matchTime: utils.getMinuteWithInjuryTime(matchTime, period),
                                    title: "Overall Possession Ratio",
                                    homeTeam: item.homeTeam.name,
                                    awayTeam: item.awayTeam.name,
                                    homeTeamPossession: Number(R.path(['values', 'home'], R.find(R.propEq("title", "% Possession"), item.stats))),
                                    awayTeamPossession: Number(R.path(['values', 'away'], R.find(R.propEq("title", "% Possession"), item.stats)))
                                };
                                return hl.sequence([
                                    utils.ps.putObject(statSnapshotURI, statSnapshot),
                                    utils.ps.add('/v1/live-centre/opta-commentary/' + fixtureId, statSnapshot.postTime, '${' + statSnapshotURI + ';null}'),
                                    utils.tickleClient('opta-commentary.' + fixtureId, 'update', {self: statSnapshotURI})
                                ]).collect().map(R.always(item));
                            }
                            else return [item];
                        });
                })
                .flatMap(function(item){
                    var uri = ['/v1/live-centre/opta/fixtures', fixtureId].join('/');
                    return hl.sequence([
                        utils.ps.put(uri, JSON.stringify(item)),
                        utils.ps.add(['/v1/live-centre/opta-fixtures', competitionId, seasonId].join('/'),
                            startDate,
                            '${' + uri + ';null}'),
                        utils.tickleClient(['opta.fixtures', fixtureId].join('.'), 'update', { self: uri })
                    ]);
                });
        });
},
    getFixtureList: function (xml) {
    return xml
        .pluck('SoccerFeed')
        .flatMap(R.prop('SoccerDocument'))
        .flatMap(function(SoccerDocument){
            var nameMap = getNameMapping(SoccerDocument.Team);
            var competitionId = parseInt(SoccerDocument.$.competition_id);
            var seasonId = parseInt(SoccerDocument.$.season_id);
            return hl(SoccerDocument.MatchData).flatMap(function (MatchData) {
                var fixtureId = parseInt(MatchData.$.uID.replace('g',''));
                var lastUpdated = utils.unixLocal(MatchData.$.last_modified);
                var date = R.path(['MatchInfo', 0, 'Date', 0], MatchData);
                var timezone = R.path(['MatchInfo', 0, 'TZ', 0], MatchData);
                var startDate = utils.unixLocal(date+' '+ timezone);
                var teamData = MatchData.TeamData.reduce(function(memo, TeamData){
                    return R.assoc(TeamData.$.Side.toLocaleLowerCase()+'Team', {
                        id: parseInt(TeamData.$.TeamRef.replace('t','')),
                        name: nameMap[TeamData.$.TeamRef],
                        penaltyShots: [],
                        goalsScored: [],
                        score: 0
                    }, memo);
                }, {});
                var uri = ['/v1/live-centre/opta/fixtures', fixtureId].join('/');
                return utils.ps.get(uri+';{ "lastUpdated": 0 }')
                    .flatMap(function(fixture){
                        if (!fixture.stats && fixture.lastUpdated < lastUpdated) {
                            return [{
                                "id": fixtureId,
                                "competitionId": competitionId,
                                "seasonId": seasonId,
                                "url": "${" + ['/v1/live-centre/opta/fixtures', fixtureId, 'article'].join('/') + ",url;}",
                                "lastUpdated": lastUpdated,
                                "startDate":  startDate,
                                "period": MatchData.MatchInfo[0].$.Period,
                                "location": R.filter(R.pathEq(['$', 'Type'], 'Venue'), MatchData.Stat || [])
                                    .map(function(Stat){ return Stat._; }).join(),
                                "groupLabel": MatchData.$.GroupName,
                                "homeTeam": teamData.homeTeam,
                                "awayTeam": teamData.awayTeam
                            }];
                        }
                        return [];
                    })
                    .flatMap(function (item){
                        return hl.sequence([
                            utils.ps.put(uri, JSON.stringify(item)),
                            utils.ps.add(['/v1/live-centre/opta-fixtures', competitionId, seasonId].join('/'),
                                startDate,
                                '${' + uri + ';null}')
                        ]);
                    });
            });
        });
}
};
