"use strict";
var R = require("ramda");
var hl = require("highland");
var Pusher = require('pusher');
var config = require('config');
var moment = require('moment-timezone');
var pusher = new Pusher(config.Pusher);
var ps;

function deepMerge(x, y) {
    for (var key in y) {
        if (y.hasOwnProperty(key)) {
            x[key] = x[key] === void 0 ? y[key] : deepMerge(x[key], y[key]);
        }
    }
    return x;
}

module.exports = {
    get ps() {
        if (R.isNil(ps)) ps = require('ps-direct-sdk')(config.redis);
        return ps;
    },

    getMinuteWithInjuryTime: function (minute, period) {
        if (R.is(String, period) && Number.isNaN(period = parseInt(period, 10))) {
            period = {
                PreMatch: 1,
                FirstHalf: 1,
                SecondHalf: 2,
                FullTime: 2,
                ExtraFirstHalf: 3,
                ExtraSecondHalf: 4
            }[period];
        }
        minute = parseInt(minute, 10);
        if (minute > 120 && period === 4) {
            return "120+" + (minute - 120);
        }
        if (minute > 105 && period === 3) {
            return "105+" + (minute - 105);
        }
        if (minute > 90 && period === 2) {
            return "90+" + (minute - 90);
        }
        if (minute > 45 && period === 1) {
            return "45+" + (minute - 45);
        }
        return isNaN(minute) ? "" : minute.toString();
    },

    deepMerge: deepMerge,

    tickleClient: function (path, eventType, item) {
        return hl(function (push) {
            pusher.trigger(path, eventType, item, null, function (err) {
                if (err) {
                    push(err);
                } else {
                    push(null, 'tickled - ' + item.self);
                }
                push(null, hl.nil);
            });
        });
    },

    getNameMapping: function (Teams) {
        return R.reduce(function (memo, Team) {
            if (Team.Player) {
                Team.Player.forEach(function (Player) {
                    memo[Player.$.uID] = Player.PersonName[0].Known ?
                        Player.PersonName[0].Known[0] :
                    Player.PersonName[0].First[0] + ' ' + Player.PersonName[0].Last[0];
                });
            }
            memo[Team.$.uID] = Team.Name[0];
            return memo;
        }, {}, Teams);
    },

    timeStampFromLocal: function (localString) {
        return moment.tz(localString, "Europe/London").format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    },

    timeStampFromGmt: function (gmtString) {
        return moment.tz(gmtString, "GMT").clone().tz("Europe/London").format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    },

    unixLocal: function (local) {
        return Number(moment.tz(local, "Europe/London").format("x"));
    },

    unixGmt: function (gmt) {
        return Number(moment.tz(gmt, "GMT").clone().tz("Europe/London").format("x"));
    }


};

