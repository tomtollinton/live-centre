"use strict";
var hl = require('highland');
var R = require('ramda');
var utils = require("./utils");

function formatF3(TeamStandings, teamNameByReference, competitionId, seasonId, lastUpdated){

    return TeamStandings.TeamRecord.map(function(TeamRecord){
        var teamId = parseInt(TeamRecord.$.TeamRef.replace('t', ''));
        return {
            "competitionId": parseInt(competitionId),
            "seasonId": parseInt(seasonId),
            "teamId": parseInt(teamId),
            "teamName": teamNameByReference[TeamRecord.$.TeamRef],
            "group": TeamStandings.Round ? TeamStandings.Round[0].Name[0].$.id : void 0,
            "played": parseInt(TeamRecord.Standing[0].Played[0]),
            "won": parseInt(TeamRecord.Standing[0].Won[0]),
            "drawn": parseInt(TeamRecord.Standing[0].Drawn[0]),
            "lost": parseInt(TeamRecord.Standing[0].Lost[0]),
            "for": parseInt(TeamRecord.Standing[0].For[0]),
            "against": parseInt(TeamRecord.Standing[0].Against[0]),
            "points": parseInt(TeamRecord.Standing[0].Points[0]),
            "goalDifference": parseInt(TeamRecord.Standing[0].For[0]) - parseInt(TeamRecord.Standing[0].Against[0]),
            "id": parseInt(teamId),
            "lastUpdated": lastUpdated
        };
    });
}

module.exports = {

    transformFeed : function(xml) {
        return xml
            .pluck('SoccerFeed')
            .flatMap(R.prop('SoccerDocument'))
            .flatMap(function (SoccerDocument) {
                var teamNameByReference = utils.getNameMapping(SoccerDocument.Team);
                var competitionId = SoccerDocument.$.competition_id;
                var seasonId = SoccerDocument.$.season_id;
                var lastUpdated = utils.unixLocal(SoccerDocument.$.timestamp);

                return hl(SoccerDocument.Competition[0].TeamStandings)
                    .flatMap(function(TeamStandings){
                        return hl(formatF3(TeamStandings, teamNameByReference, competitionId, seasonId, lastUpdated))
                            .map(function (item){
                                var uri = '/v1/live-centre/opta/standings/'+ item.id;
                                return hl.sequence([
                                    utils.ps.put(uri, JSON.stringify(item)),
                                    utils.ps.add('/v1/live-centre/opta-standings/' + item.competitionId + '/' + item.seasonId, item.id, '${' + uri + ';null}'),
                                    utils.tickleClient('opta-standings.'+item.competitionId + '.' + item.seasonId, 'update', {
                                        self: uri
                                    })
                                ]);
                            });
                    });
            })
            .merge();
    }
};
