"use strict";
var R = require('ramda');
var hl = require('highland');
var config = require('config');
var ps = require('ps-direct-sdk')(config.redis);
var Pusher = require('pusher');
var pusher = new Pusher(config.Pusher);
var dedynamize = require('dynamodb-data-types').AttributeValue.unwrap;
var presentation = require('./presentation');
var stringify = require('./stringify');
var BeneLogger = require('bene-logger');
var logger = new BeneLogger();

var tickleClient = function(path, eventType, item) {
    return hl(function(push) {
        pusher.trigger(path, eventType, item, null, function(err) {
            if (err) push(err);
            else push(null, 'tickled - ' + item.self);
            push(null, hl.nil);
        });
    });
};

function hlTimed(msg, params, xs) {
    logger.time(msg);
    return xs.tap(() => { logger.timeEnd(msg, params); });
}

// isPublished :: Object -> Boolean
function isPublished(item) {
    return item._presentation && item.state === 'published' ||
        (item.sourceType === 'publication' && item.state !== 'deleted');
}

// createPusherChannel :: Object -> [String]
function createPusherChannels(item) {
    var channels = [item.source + "-live." + item.id];
    if(R.is(Number, item._presentation.fixtureId)) {
        channels.push('opta-commentary.' + item._presentation.fixtureId);
    }
    return channels;
}

// createLiveEventActions :: PresentationServiceAction ps => :: Object -> [Stream ps]
function createLiveEventActions(item) {
    var actions = [];
    if(item.type === "live-event"){
        if(R.is(Number, item._presentation.fixtureId)) {
            actions.push(ps.put('/v1/live-centre/opta/fixtures/' + item._presentation.fixtureId + '/article', '{ "url": "${' + item.self+ ',url;}" }'));
        }
        actions.push(ps.put('/v1/live-centre/' + item.source + '-live/' + item.id + '/pusher',
            '{"channels":' + JSON.stringify(createPusherChannels(item)) + ',"encrypted":${/v1/live-centre/pusher/encrypted},'+
            '"key":"${/v1/live-centre/pusher/key}","appId":"${/v1/live-centre/pusher/appId}"}'));
    }
    return actions;
}

// createLiveEventEntriesActions :: PresentationServiceAction ps => :: Object -> [Stream ps]
function createLiveEventEntriesActions(item) {
    var actions = [];
    if (item.type === 'live-event-entry') {
        const params = {
            article_id: item.id,
            article_source: item.source,
            event_id: item._presentation.event
        };
        if (item._presentation && item.state === 'published') {
            actions.push(
                hlTimed(
                    'lambda invoked PUT complete',
                    params,
                    ps.get(item.self + ';false')
                        .map(JSON.parse)
                        .flatMap(priorEntry => {
                            if (priorEntry !== false) {
                                logger.info('updating existing entry', params);
                                item._presentation.postTime = priorEntry.postTime;
                                item._presentation.createdDate = priorEntry.createdDate;
                            }
                            return ps.add('/v1/live-centre/' + item._presentation.event, item._presentation.postTime, '${' + item.self + ';null}');
                        })
                )
            );
            actions.push(
                hlTimed(
                    'lambda pusher tickle PUT complete',
                    params,
                    tickleClient(item._presentation.event.replace('/', '.'), 'update', {
                        self: item.self
                    })
                )
            );
        } else {
            actions.push(
                hlTimed(
                    'lambda invoked DELETE complete',
                    params,
                    ps.rem('/v1/live-centre/' + item._presentation.event, '${' + item.self + ';null}')
                )
            );
            actions.push(
                hlTimed(
                    'lambda pusher tickle DELETE complete',
                    params,
                    tickleClient(item._presentation.event.replace('/', '.'), 'delete', {
                        self: item.self
                    })
                )
            );
        }
    }
    return actions;
}

// createPublicationActions :: PresentationServiceAction ps => :: Object -> [Stream ps]
function createPublicationActions(item) {
    return item.sourceType === 'publication' ?
        [ps.put('/v1/live-centre/pusher/appId', config.Pusher.appId),
            ps.put('/v1/live-centre/pusher/key', config.Pusher.key),
            ps.put('/v1/live-centre/pusher/encrypted', "true")]
        : [];
}

// createUpsertAction :: PresentationServiceAction ps => :: Object -> Stream ps
function createUpsertAction(item) {
    const params = {
        article_id: item.id,
        article_source: item.source
    };
    if (item.type === 'live-event-entry') {
        return hlTimed(
            'lambda invoked UPSERT complete',
            {},
            ps.get(item.self + ';false')
                .map(JSON.parse)
                .flatMap(priorEntry => {
                    if(priorEntry !== false) {
                        logger.info('updating existing entry', R.merge(params, {
                            event_id: item._presentation.event
                        }));
                        item._presentation.postTime = priorEntry.postTime;
                        item._presentation.createdDate = priorEntry.createdDate;
                    }
                    return ps.put(item.self, R.is(String, item._presentation) ? item._presentation : stringify(item.source, item._presentation));
                })
        );
    }
    return hlTimed(
        'lambda invoked UPSERT complete',
        params,
        ps.put(item.self, R.is(String, item._presentation) ? item._presentation : stringify(item.source, item._presentation))
    );
}

exports.handler = function(event, context, done) {
    const eventIds = R.pipe(
        R.pluck('dynamodb'),
        R.pluck('Keys'),
        R.map((keys) => {
            return [R.path(['Source', 'S'])(keys), R.path(['Id', 'N'])(keys)].join('/');
        })
    )(event.Records);

    logger.setDefaults({
        request_id: context.awsRequestId,
        event_ids: eventIds,
        service_name: 'liveEventEntryConsumer'
    });

    logger.info('processing lambda');
    logger.time('processed lambda');

    var grouped = R.groupBy(function(record) {
        var dbKeys = dedynamize(record.dynamodb.Keys);
        return dbKeys.Id + dbKeys.Source;
    }, event.Records);

    hl.values(grouped)
        .map(R.map(function(record) {
            var image = dedynamize(record.dynamodb.NewImage || record.dynamodb.OldImage);
            var output = presentation.getMeta(image);

            logger.info('generating actions', {
                sequence_id: record.dynamodb.SequenceNumber,
                article_id: image.Id,
                article_source: image.Source
            });

            if (!record.dynamodb.NewImage) output.state = 'deleted';
            return output;
        }))
        .map(function (xs) {
            return R.reduce(function(acc, item) {
                var actions = [];
                if (isPublished(item)) {
                    actions = actions.concat([createUpsertAction(item)])
                        .concat(createLiveEventActions(item))
                        .concat(createPublicationActions(item))
                        .concat(createLiveEventEntriesActions(item));
                } else {
                    actions = actions.concat([ps.del(item.self)])
                        .concat(createLiveEventEntriesActions(item));
                }
                return acc.concat([hl.sequence(actions)]);
            }, [], xs);
        })
        .map(hl.sequence)
        .merge()
        .errors(err => {
            logger.error('lambda errored', {
                err_stack: err.stack,
                err_msg: err.message
            });
        })
        .collect()
        .toCallback(function (err, data) {
            logger.timeEnd('processed lambda');
            context.callbackWaitsForEmptyEventLoop = false; // prevent event loop lingering on and causing lambda to timeout
            done(err, data);
        });
};