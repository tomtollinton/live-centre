"use strict";
var R = require('ramda');
var cheerio = require('cheerio');

function parseText(markup) {
    
    //console.log('parseText',markup);
    
    var $ = cheerio.load(markup);
    var ele = $.root().children().first();
    var o = null;

    //scripts come with twitter
    if (ele.is('script')) {
        //mark for ignore
        o = "IGNORE";
    }
    //match any list / table
    if (ele.is("ul,ol,table,h1,h2,h3,iframe")) {

        o = {
            type: "html",
            text: markup
        };
    }

    //match top level escenic image
    if (ele.is("img[id]")) {
        o = {
            type: 'escenic',
            id: exports.findId(ele.attr("src").toString())
        };
    }

    //twitter raw markup block
    if (ele.is('blockquote.twitter-tweet') || ele.is("blockquote.twitter-video")) {
        o = {
            type: 'tweet',
            url : ele.find('a').last().attr('href')
        };
    } else if (ele.is('blockquote.instagram-media')) {
        o = {
            type: 'instagram',
            url: ele.find ('a').attr ( 'href' )
        };
    } else if (ele.is('blockquote')) {
        o = {
            type: 'blockquote',
            cite: ele.attr('cite') || "",
            text: ele.html().trim()
        };
    }

    //twitter link (live center)
    if ( ele.is ( 'a' ) && ! R.isNil ( ele.attr ( 'href' ) && ele.attr ( 'href' ).match ( /http(s)?:\/\/(?:www\.)?twitter\.com\/([^\/]+)\/status\/(\d+)/ )) ) {
        o = {
            type: 'tweet',
            url: ele.attr ( 'href' )
        };
    }

    //paragraph element
    if (ele.is('p')) {

        var escenicImageLive = ele.find("img.esc-content").first();

        //If the user directly copy-pastes text from content studio into live center, they will get a weservice link for images.
        var escenicWebserviceImage = (ele.find("img").attr('src') || '').match(/\/webservice\/thumbnail\/article\/\d+$/);

        var escenicElement = ele.find("a[id]").first();

        var escenicElementLive = ele.find('a[class^="esc"]').first();

        // Escenic tags twitter and instagram social embeds with a Twitter tag
        const escenicSocial = ele.find('a.esc-tag-Twitter').attr('href');
        
        var tweetMatch = ele.html().match(/(?:<|\&lt;)blockquote(?:.|[\r\n])*?class=(?:\\"|&quot;)twitter-tweet(?:\\"|&quot;)(?:.|[\r\n])*?(?:<|\&lt;)p.*?(?:>|\&gt;)(?:.|[\r\n])*?(twitter\.com\/\w+\/status\/\d+)(?:\\"|&quot;)(?:.|[\r\n])*?(?:>|\&gt;)(?:.|[\r\n])*?blockquote(?:>|\&gt;)/);

        var tweetSciptMatch = ele.html().match(/&lt;script async src=&quot;\/\/platform.twitter.com\/widgets.js&quot; charset=&quot;utf-8&quot;&gt;&lt;\/script&gt;/m);

        var tweetLinkMatch = ( ele.find ('a').attr ( 'href' ) || '' ).match ( /http(s)?:\/\/(?:www\.)?twitter\.com\/([^\/]+)\/status\/(\d+)/ );

        var instagramLinkMatch = ( ele.find ('a').attr ( 'href' ) || '' ).match ( /http(s)?:\/\/(?:www\.)?instagram\.com\/p\/([^\/]+)/ );

        var tweetLinkMatchWithClass = ele.find ( 'a.twitter' ) && ( ele.find ('a').attr ( 'href' ) || '' ).match ( /http(s)?:\/\/(?:www\.)?twitter\.com\/([^\/]+)\/status\/(\d+)/ );

        if(escenicSocial) {
            const url = escenicSocial.toString();
            o = {
                type: R.contains('twitter.com', url) ? 'tweet' : 'instagram',
                url: url
            };
        } else if ( tweetLinkMatchWithClass ) {
            o = {
                type: 'tweet',
                url: tweetLinkMatchWithClass[0]
            };
        } else if ( tweetLinkMatch ) {
            o = {
                type: 'tweet',
                url : tweetLinkMatch[3]
            };
        } else if ( instagramLinkMatch ) {
            o = {
                type: 'instagram',
                url: ele.find ('a').attr ( 'href' )
            };
        } else if (escenicElement.length) {
            o = {
                type: 'escenic',
                url: escenicElement.attr("href").toString(),
                id: exports.findId(escenicElement.attr("href").toString())
            };
        } else if (escenicElementLive.length) {
            o = {
                type: 'escenic',
                url: escenicElementLive.attr("href").toString(),
                id: exports.findId(escenicElementLive.attr("href").toString())
            };
        } else if (escenicImageLive.length) {
            o = {
                url: escenicImageLive.attr("src"),
                type: 'image',
                caption: escenicImageLive.attr("title"),
                altText: escenicImageLive.attr("alt"),
                id: exports.findId(escenicImageLive.attr("src"))
            };
        } else if (escenicWebserviceImage) {
            //When we get a webservice link, we don't get any more useful information than the id.
            var id = exports.findId(escenicWebserviceImage[0]);
            o = {
                type: 'image',
                caption: '',
                altText: '',
                id: id
            };
        } else if (tweetMatch) {
            o = {

                type: 'tweet',
                url: 'https://' + tweetMatch[1]
            };
        } else if (tweetSciptMatch  || ele.html().trim() === '' || ele.html().trim() === '&#xA0;') {
            //mark for ignore
            o = "IGNORE";
        } else {
            o = {
                type: "text",
                text: ele.html()
            };
        }
    }


    if (!o) {
        o = {
            type: "text",
            text: markup
        };
        //throw new Error("Unmatched element :" + ele);
    }

    return o == null ? 'IGNORE' : o;
}

exports.parse = R.pipe(
    R.replace(/<script async.*?\/>/g, ''),
    parser,
    R.map(parseText),
    R.reject(R.equals("IGNORE"))
);

function parser(input) {
    var $ = cheerio.load(input);

    var coll = $.root().children().map(function(i, el) {
        return $.html(el).toString();
    }).get();

    return coll;
}

exports.idAfterContent = R.match(/content\/(\d+)$/);

exports.idFromInternal = R.match(/article\/(\d+)$/);

exports.idAtEOL = R.match(/-(\d+)$/);

exports.idAtExtension = R.match(/article(\d+)\.ece/);

exports.findId = function(str) {
    var id;
    if (!R.isEmpty(id = exports.idFromInternal(str))) {
        return parseInt(id[1], 10);
    }

    if (!R.isEmpty(id = exports.idAtExtension(str))) {
        return parseInt(id[1], 10);
    }

    if (!R.isEmpty(id = exports.idAtEOL(str))) {
        return parseInt(id[1], 10);
    }

    if (!R.isEmpty(id = exports.idAfterContent(str))) {
        return parseInt(id[1], 10);
    }

    return str;
};
