"use strict";
var R = require('ramda');

module.exports = function stringify(source, presentation) {
    var strings;
    if (R.is(Array, presentation)) {
        strings = R.map(function(item) {
            if (item && item.type === 'escenic') return '${/v1/live-centre/' + source + '-article/' + item.id + ';null}';
            else return stringify(source, item);
        }, presentation);
        return '[' + strings.join(', ') + ']';
    } else if (R.is(Object, presentation)) {
        if (presentation && presentation.$ref) return presentation.$ref;
        strings = R.map(function(pair) {
            if (R.isNil(pair[1])) return null;
            else return pair.map(R.partial(stringify, [source])).join(': ');
        }, R.reject(function(pair) {
            return R.isNil(pair[1]);
        }, R.toPairs(presentation)));
        return '{' + strings.join(', ') + '}';
    } else if (presentation === null) {
        return "null";
    } else return JSON.stringify(presentation);
};