const chai = require('chai');
const assert = chai.assert;
const rewire = require('rewire');
const fs = require('fs');
const R = require('ramda');
const hl = require('highland');
const sinon = require('sinon');

const lambda = rewire('../index.js');
const When = require('./records.json');

const config = lambda.__get__('config');
const dedynamize = lambda.__get__('dedynamize');

//stub out aws and pusher
var pusher = [];
const ps =  lambda.__get__('ps');
ps.getJson = key => {
    return ps.get(key).map(JSON.parse);
};

//The lambda is very noisy
lambda.__set__('console', {log:R.always(), error:R.always(), time:R.always(), timeEnd:R.always()});
lambda.__set__('pusher', {
    trigger: (channel, event, data, socketId, cb) => {
        pusher_trigger = true;
        assert(R.is(String, channel));
        assert(R.is(String, event));
        assert(R.is(Object, data));
        pusher =  pusher || [];
        pusher.push(data);
        cb();
    }
});

const runHandler = hl.wrapCallback((event, done) => {
    lambda.handler(event, {
        done,
        fail: done,
        succeed: done.bind(null, null),
        awsRequestId: 1337
    }, done);
});

config.Pusher = {
    "appId": "151632",
    "key": "12e0896f7b5f498183a4",
    "secret": "410103531f8ff0bb21a0"
};

function getSimpleEntry(articleId, entryId) {
    if(R.isNil(entryId)) entryId = 16032;
    return R.assocPath(['dynamodb', 'Keys', 'Id', 'N' ], entryId.toString(), R.assocPath(['dynamodb', 'NewImage', 'Id', 'N' ], entryId.toString(), R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M', 'eventId', 'N' ], articleId.toString(), When["All fields filled"])));
}

describe('component tests', () => {
    before(() => {
        var origStdoutWrite = process.stdout.write;
        var logFilterPattern = /(info\:)|(ResourceNotFoundError)/;

        //filter log output
        sinon.stub(process.stdout, 'write', function() {
            var args = Array.prototype.slice.call(arguments);
            if (!logFilterPattern.test(args[0])) {
                return origStdoutWrite.apply(process.stdout, args);
            }
        });
    });

    after(() => process.stdout.write.restore());

    describe('scenarios', () => {
        it('Sending a new entry with all fields populated', (done) => {
            runHandler({Records: [getSimpleEntry(100001)]})
                .flatMap(() => ps.getJson('/v1/live-centre/nationals-live/100001'))
                .toCallback((err, page) => {
                    assert.ok(page, 'page is saved in ps');
                    assert(page.length === 1, 'page has an entry');
                    assert(pusher);
                    done(err);
                });

        });

        it('should put the item into the presentation service before the pusher tickle', (done) => {
            runHandler({ Records: [getSimpleEntry(100001)] })
                .collect()
                .toCallback((err, data) => {
                    assert.notOk(err);
                    assert(R.contains('tickled', R.pipe(R.flatten, R.last)(data)));
                    done();
                });
        });

        it('Sending a new entry with all optional fields missing', (done) => {
            runHandler({ Records: [When["All optional fields missing"]] })
                .collect()
                .toCallback((err, data) => {
                    if (err) return done(err);
                    ps.getJson('/v1/live-centre/nationals-live/100002')
                        .pull(function(err, page){
                            assert.notOk(err);
                            assert.ok(page, 'page is saved in ps');
                            assert(page.length === 1, 'page has an entry');
                            assert(pusher);
                            assert(page[0].prefix === void 0, 'prefix not present');
                            assert.equal(page[0].author, void 0);
                            assert(page[0].title === void 0, 'title not present');
                            done();
                        });
                });
        });

        it('Sending two entries orders them by publish date when sent out of order', (done) => {
            runHandler({
                    Records: [
                        R.assocPath(['dynamodb', 'NewImage', 'PublishTime', 'N'], "2", getSimpleEntry(110001, 1)),
                        R.assocPath(['dynamodb', 'NewImage', 'PublishTime', 'N'], "1", getSimpleEntry(110001, 2))
                    ]
                })
                .collect()
                .toCallback((err, data) => {
                    if (err) return done(err);
                    ps.getJson('/v1/live-centre/nationals-live/110001')
                        .pull(function(err, page) {
                            assert.notOk(err);
                            assert.ok(page, 'page is saved in ps');
                            assert.strictEqual(page.length, 2);
                            assert.strictEqual(page[0].id, 2, 'first entry should be entry with earliest publish time');
                            done();
                        });
                });
        });

        it('Sending two entries orders them by publish date', (done) => {
            runHandler({
                    Records: [
                        R.assocPath(['dynamodb', 'NewImage', 'PublishTime', 'N'], "1", getSimpleEntry(110002, 1)),
                        R.assocPath(['dynamodb', 'NewImage', 'PublishTime', 'N'], "2", getSimpleEntry(110002, 2))
                    ]
                })
                .collect()
                .toCallback((err, data) => {
                    if (err) return done(err);
                    ps.getJson('/v1/live-centre/nationals-live/110002')
                        .pull(function(err, page) {
                            assert.notOk(err);
                            assert.ok(page, 'page is saved in ps');
                            assert.strictEqual(page.length, 2);
                            assert.strictEqual(page[0].id, 1, 'first entry should be entry with earliest publish time');
                            done();
                        });
                });
        });

        it('Updating an entry doesn\'t update order of entries or the createdDate', (done) => {
            runHandler({
                    Records: [
                        R.assocPath(['dynamodb', 'NewImage', 'PublishTime', 'N'], "123", getSimpleEntry(110003, 1)),
                        R.assocPath(['dynamodb', 'NewImage', 'PublishTime', 'N'], "222", getSimpleEntry(110003, 2)),
                        R.assocPath(['dynamodb', 'NewImage', 'PublishTime', 'N'], "321", getSimpleEntry(110003, 1))
                    ]
                })
                .collect()
                .toCallback((err, data) => {
                    if (err) return done(err);
                    ps.getJson('/v1/live-centre/nationals-live/110003')
                        .pull(function(err, page) {
                            assert.notOk(err);
                            assert.ok(page, 'page is saved in ps');
                            assert.strictEqual(page.length, 2);
                            assert.strictEqual(page[0].id, 1, 'first entry should be first entry published');
                            //Using test of milliseconds to avoid any nonsense with timezones.
                            assert(R.test(/123/, page[0].createdDate), 'createdDate should remain the same on update of entry');
                            done();
                        });
                });
        });

        it('Sending 30 entries in a row', (done) => {
            pusher = [];
            const records = R.map(i => getSimpleEntry(100004, i), R.range(0, 30));

            runHandler({
                Records: records
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/100004')
                    .pull((err, page) => {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.length, 30, 'page has 30 entries');
                        assert.equal(pusher.length, 30);
                        done(err)
                    });
            });
        }).timeout(3000);

        it('Submitting a new entry', (done) => {
            runHandler({
                Records: [When["Submit nationals-live/100005/16033"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/100005;[]')
                    .pull(function(err, page){
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.length, 0, 'page has no entry');
                        done();
                    });
            });
        });

        it('Approving a submitted entry', (done) => {
            runHandler({
                Records: [When["Approve nationals-live/100005/16033"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/100005')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        done();
                    })
            });
        });

        it('Sending an entry with sticky', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M', 'sticky', 'BOOL'], true, getSimpleEntry(100006))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/100006')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert(page.length === 1, 'page has an entry');
                        assert(page[0].isSticky === true, 'page has the entry modified');
                        done();
                    });
            });
        });

        it('Updating the prefix of an entry', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','prefix', 'S'], 'new prefix', getSimpleEntry(100007))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/100007')
                    .pull(function(err, page){
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert(page[0].prefix === 'new prefix', 'page has the entry modified');
                        done();
                    });
            });
        });

        it('Updating the hideTimestamp attribute of an entry', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','hideTimestamp', 'S'], 'true', getSimpleEntry(100008))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/100008')
                    .pull(function(err, page){
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].hideTimestamp, true, 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the title of an entry', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M', 'title', 'S'], 'my new title', getSimpleEntry(100009))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/100009')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].title, 'my new title', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the body of an entry', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','body', 'S'], '<p>nice body</p>', getSimpleEntry(1000010))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000010')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].content[0].text, 'nice body', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the author of an entry', (done) => {
            runHandler({
                Records: [When["Set author"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000011')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].author, 'Bob Geldof', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the entry style to �Key Event�', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','entryStyle', 'S'], 'keyEvent', getSimpleEntry(1000012))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000012')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].type, 'key-event', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the entry style to �Breaking News�', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','entryStyle', 'S'], 'breakingNews', getSimpleEntry(1000013))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000013')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].type, 'breaking-news', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the entry style to �Tweet�', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','entryStyle', 'S'], 'tweet', getSimpleEntry(1000014))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000014')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].type, 'tweet', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the entry style to �Goal�', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','entryStyle', 'S'], 'goal', getSimpleEntry(1000015))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000015')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].type, 'goal', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the entry style to �Red Card�', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','entryStyle', 'S'], 'redCard', getSimpleEntry(1000016))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000016')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].type, 'red-card', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Updating the entry style to �Yellow Card�', (done) => {
            runHandler({
                Records: [R.assocPath(['dynamodb', 'NewImage', 'Fields', 'M','entryStyle', 'S'], 'yellowCard', getSimpleEntry(1000017))]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000017')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 1, 'page has an entry');
                        assert.equal(page[0].type, 'yellow-card', 'page has the entry modified');
                        done();
                    })
            });
        });

        it('Deleting an entry for an event with a single entry', (done) => {
            runHandler({
                Records: [When["Delete nationals-live/6679834/16033"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/6679834')
                    .pull(function(err, page) {
                        // this test should've always returned a 404 as the resource no longer
                        // exists on redis (as opposed to empty array)
                        assert.ok(err);
                        assert.strictEqual(err.type, 'KeyNotFound');
                        assert.strictEqual(err.message, '/v1/live-centre/nationals-live/6679834 not available');
                        done();
                    })
            });
        });

        it('Deleting the most recent entry for an event with multiple updates', (done) => {
            var initialRecords = R.map(i => getSimpleEntry(1000018, i), R.range(0, 30));
            var record = getSimpleEntry(1000018, 29);
            record.dynamodb.OldImage = record.dynamodb.NewImage;
            delete record.dynamodb.NewImage;
            runHandler({
                Records: R.append(record, initialRecords)
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000018')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 29, 'page has exactly one entry deleted');
                        assert(!R.find((x) => x.id === 29, page), 'page has last entry deleted');
                        done();
                    })
            });
        });

        it('Deleting the least recent entry for an event with multiple updates', (done) => {
            var initialRecords = R.map(i => getSimpleEntry(1000019, i), R.range(0, 30));
            var record = getSimpleEntry(1000019, 0);
            record.dynamodb.OldImage = record.dynamodb.NewImage;
            delete record.dynamodb.NewImage;
            runHandler({
                Records: R.append(record, initialRecords)
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000019')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 29, 'page has exactly one entry deleted');
                        assert(!R.find((x) => x.id === 0, page), 'page has first entry deleted');
                        done();
                    })
            });
        });

        it('Deleting the middle entry for an event with multiple updates', (done) => {
            var initialRecords = R.map(i => getSimpleEntry(1000020, i), R.range(0, 30));
            var record = getSimpleEntry(1000020, 15);
            record.dynamodb.OldImage = record.dynamodb.NewImage;
            delete record.dynamodb.NewImage;
            runHandler({
                Records: R.append(record, initialRecords)
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/1000020')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert(page.length === 29, 'page has exactly one entry deleted');
                        assert(!R.find((x) => x.id === 15, page), 'page has middle entry deleted');
                        done();
                    });
            });
        });

        it('Creates an image presentation', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["New Section: 70"], When["New Image: 6679831"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-article/6679831')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.type, 'image');
                        assert.equal(page.caption, "Kate Wright at Michael Hassini's James Bond 21st birthday party at Sugar Hut");
                        assert.equal(page.altText, "Kate Wright at Michael Hassini's James Bond 21st birthday party at Sugar Hut");
                        assert.equal(page.imageCredit, 'WENN.com');//TODO What URL?
                        assert.equal(page.src, 'http://www.mirror.co.uk/incoming/article6679831.ece/ALTERNATES/s510b/Michael-Hassinis-James-Bond-21st-birthday-party-at-Sugar-Hut.jpg');
                        done();
                    })
            });
        });

        it('Creates an live-event presentation', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["New Section: 70"], When["New live-event: 6679835"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-article/6679835')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        done();
                    })
            });
        });

        it('Creates a section presentation', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["New Section: 71"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.get('/v1/live-centre/nationals-section/71')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page, 'http://www.mirror.co.uk/news/');
                        done();
                    })
            });
        });

        it('Creates a section presentation', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["New Section: 70"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.get('/v1/live-centre/nationals-section/70')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page, 'http://www.mirror.co.uk/incoming/');
                        done();
                    })
            });
        });

        it('should delete a section presentation', (done) => {
            runHandler({
                Records: [When["Delete Section: 70"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-section/70')
                    .pull(function(err) {
                        assert.ok(err);
                        assert.strictEqual(err.type, 'KeyNotFound');
                        assert.strictEqual(err.message, '/v1/live-centre/nationals-section/70 not available');
                        done();
                    })
            });
        });

        it('Creates a publication presentation', (done) => {
            runHandler({
                Records: [When["New Publication: 2"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.get('/v1/live-centre/nationals-publication/2')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page, 'http://www.mirror.co.uk');
                        done();
                    })
            });
        });

        it('should delete a publication presentation', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["Delete Publication: 2"]]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-publication/2')
                    .pull(function(err) {
                        assert.ok(err);
                        assert.strictEqual(err.type, 'KeyNotFound');
                        assert.strictEqual(err.message, '/v1/live-centre/nationals-publication/2 not available');
                        done();
                    })
            });
        });

        it('Make a post with an image, caption and altText modified', (done) => {
            var record = R.assocPath(
                ['dynamodb', 'NewImage', 'Fields', 'M', 'body', 'S' ],
                "<p><img src=\"internal:/article/6679831\" alt=\"and modified the altText\" title=\"I have modified the caption\" class=\"esc-content esc-tag-escenic-picture esc-article-id-6679831 esc-title-Caption\" /><br /></p>",
                When["publish nationals-live/6679835/16162 with image"]
            );
            runHandler({
                Records: [When["New Publication: 2"], When["New Section: 70"], When['New Image: 6679831'], record]
            })
            .collect()
            .toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/6679835/16162')
                    .pull((err, page) => {
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.content[0].type, 'image');
                        assert.equal(page.content[0].imageCredit, 'WENN.com');
                        assert.equal(page.content[0].caption, 'I have modified the caption');
                        assert.equal(page.content[0].altText, 'and modified the altText');
                        assert.equal(page.content[0].url, 'http://www.mirror.co.uk/incoming/article6679831.ece/ALTERNATES/s510b/Michael-Hassinis-James-Bond-21st-birthday-party-at-Sugar-Hut.jpg');
                        done();
                    });
            });
        });

        it('should make a post with an html list that contains an escenic link', (done) => {
            runHandler({
                Records: [
                    When["New Publication: 2"],
                    When["New Section: 70"],
                    When['Production nationals-article/7744114 (CRON-717)'],
                    When["publish nationals-live/6679835/16163 with html containing escenic links"]
                ]
            })
                .collect()
                .toCallback((err, data) => {
                    if (err) return done(err);
                    ps.getJson('/v1/live-centre/nationals-live/6679835/16163')
                        .pull((err, page) => {
                            assert.ok(page, 'page is saved in ps');
                            assert.strictEqual(page.content[0].text,
                                "<ul><li><a href=\"http://www.mirror.co.uk/incoming/leicester-vs-west-ham-live-7744114\" class=\"esc-content esc-tag-escenic-story esc-article-id-7744114\">Embedded Html Test</a><br></li></ul>")
                            done();
                        });
                });
        });

        it('should handle post with embedded image that has a special char in the credit', done => {
            runHandler({
                Records: [
                    When["New Publication: 2"],
                    When["New Section: 70"],
                    When['Special Char nationals-live/7500354'],
                    When["Special Char nationals-live/6679835/16164"]
                ]
            })
                .collect()
                .toCallback((err, data) => {
                    if (err) return done(err);
                    ps.getJson('/v1/live-centre/nationals-live/6679835/16164')
                        .pull((err, page) => {
                            assert.ok(page, 'page is saved in ps');
                            assert.strictEqual(page.content[0].imageCredit, "Tony O'Brien\n");
                            done();
                        });
                });
        });

        it('Post with jpg, png, and gif', (done) => {
            runHandler({
                Records: [
                    When["New Publication: 2"],
                    When["New Section: 70"],
                    When["Picture jpg 6677933"],
                    When["Picture png 6679844"],
                    When["Picture gif 6647366"],
                    When["Post with jpg, png, and gif"]
                ]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/6679945/16338')
                    .pull((err, page) => {
                        assert.ok(page, 'page is saved in ps');
                        assert(page.content.length === 3, 'all images parsed');
                        page.content.forEach(image => assert.strictEqual(image.type, 'image'));
                        assert.strictEqual(page.content[0].url, 'http://www.mirror.co.uk/incoming/article6677933.ece/ALTERNATES/s510b/Screen-Shot-2015-10-21-at-194851.png');
                        assert.strictEqual(page.content[0].imageCredit, 'Henri Cartier-Bresson');
                        assert.strictEqual(page.content[1].url, 'http://www.mirror.co.uk/incoming/article6679844.ece/ALTERNATES/s510b/Kim-Kardashian-and-Kanye-West.jpg');
                        assert.strictEqual(page.content[1].imageCredit, 'AKM-GSI / Splash News');
                        assert.strictEqual(page.content[2].url, 'http://www.mirror.co.uk/incoming/article6647366.ece/ALTERNATES/s510b/world-champion-egg-balancer.gif');
                        assert.strictEqual(page.content[2].imageCredit, 'Robert Capa');
                        assert(pusher_trigger);
                        done();
                    });
            });
        });

        it('should handle images from production (CRON-717)', (done) => {
            runHandler({
                Records: [
                    When["New Publication: 2"],
                    When["New Section: 70"],
                    When["Production Image 7773997 (CRON-717)"],
                    When["Production nationals-article/7744114 (CRON-717)"],
                    When["Production nationals-live/7744114/54615 with image (CRON-717)"]
                ]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/7744114/54615')
                    .pull((err, page) => {
                        assert.ok(page, 'page is saved in ps');
                        assert(page.content.length === 4, 'all items parsed');
                        const image = page.content[3];
                        assert.strictEqual(image.type, "image");
                        assert.strictEqual(image.id, 7773997);
                        assert.strictEqual(image.url, "http://www.mirror.co.uk/incoming/article7773997.ece/ALTERNATES/s510b/Leicester-City-v-West-Ham.jpg");
                        assert.strictEqual(image.caption, "Dimitri Payet arrives");
                        assert.strictEqual(image.imageCredit, "Plumb Images/Leicester City FC via Getty");
                        assert.strictEqual(image.altText, "Dimitri Payet arrives");
                        assert(pusher_trigger);
                        done();
                    });
            });
        });

        it('should handle post with embedded tweet', (done) => {
            runHandler({
                Records: [When["Post with embedded tweet"]]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/6679945/16610')
                    .pull((err, page) => {
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.content[1].type, 'tweet');
                        assert(pusher_trigger);
                        done();
                    });
            });
        });

        it('Post with embedded instagram', (done) => {
            runHandler({
                Records: [When["Post with embedded instagram"]]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/6679945/16665')
                    .pull((err, page) => {
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.content[1].type, 'instagram');
                        assert(pusher_trigger);
                        done();
                    });
            });
        });

        it('Post with Generic Escenic Embed', (done) => {
            runHandler({
                Records: [When["Post with Generic Escenic Embed"]]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.put('/v1/live-centre/nationals-article/6676008', '{"embed":"embed"}')
                    .flatMap(() => ps.getJson('/v1/live-centre/nationals-live/6679945/16685'))
                    .pull((err, page) => {
                        assert(page.content[1].embed === "embed", 'it has a reference for the embed');
                        assert(pusher_trigger);
                        done();
                    });
            });
        });

        it('Creates a html presentation', (done) => {
            runHandler({
                Records: [When["New HTML Embed"]]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-article/6676935')
                    .pull((err, page) => {
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.type, 'html');
                        assert(R.is(String, page.text), 'element has the html content');
                        done();
                    });
            });
        });

        it('Creates a fixture-article presentation', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["New Section: 70"], When["New live match event"]]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/opta/fixtures/755526/article')
                    .pull((err, page) => {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.url, 'http://www.mirror.co.uk/incoming/test-live-event-cron-313-6682005');
                        done();
                    });
            });
        });

        it('Creates the pusher credentials for live events', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["New live match event"]]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/6682005/pusher')
                    .pull((err, page) => {
                        assert.equal(page.appId, "151632");
                        assert.equal(page.key, "12e0896f7b5f498183a4");
                        assert.equal(page.encrypted, true);
                        assert.deepEqual(page.channels, ["nationals-live.6682005","opta-commentary.755526"]);
                        done();
                    });
            });
        });

        it('Creates the pusher channel endpoints for regionals live event', (done) => {
            runHandler({
                Records: [When["New Publication: 2"], When["New regionals live-event: 6679836"]]
            }).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/regionals-live/6679836/pusher')
                    .pull(function (err, pusherCredentials){
                        assert.notOk(err);
                        assert.deepEqual(pusherCredentials, {
                            "appId": "151632",
                            "key": "12e0896f7b5f498183a4",
                            "channels": [
                                "regionals-live.6679836"
                            ],
                            "encrypted": true
                        });
                        done();
                    });
            });
        });
    });
    describe('network latency', () => {
        it('should execute batched updates to same entry sequentially', done => {
            const _put = ps.put;
            ps.put = (key, val) => {
                return hl((push, next) => {
                    const rand = Math.floor(Math.random() * 200) + 200;
                    const text = JSON.parse(val).content[0].text;
                    // simulate network latency
                    setTimeout(() => {
                        _put(key, val).toArray(() => {
                            push(null, hl.nil);
                        });
                    }, text === "update 6" ? 150 : rand)
                })
            };
            runHandler(When['Multiple updates to same event']).collect().toCallback((err, data) => {
                if (err) return done(err);
                ps.getJson('/v1/live-centre/nationals-live/7484503/46163')
                    .pull(function(err, page) {
                        assert.notOk(err);
                        assert.ok(page, 'page is saved in ps');
                        assert.equal(page.content[0].text, 'update 6');
                        ps.put = _put;
                        done();
                    });
            });
        });
    });

    describe("errors should be caught and not cause the lambda to be blocked", () => {
        lambda.__set__("logger.error", sinon.stub());
        it("catchs lambda errors", (done) => {
            runHandler({
                "Records": [When["Causes Error"]]
            })
                .collect()
                .toCallback((err) => {
                    var logger = lambda.__get__("logger.error");
                    assert.isTrue(logger.calledWith("lambda errored"));
                    done(err); // error should not get to callback function
            })
        })
    });
});

