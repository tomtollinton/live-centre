var chai = require('chai');
var assert = chai.assert;
var rewire = require('rewire');
var fs = require('fs');
var R = require('ramda');

describe('unit tests', () => {
    describe('contentTypeParser', () => {
        var tests = require('./test_cases.json');
        var contentTypeParser = require('../contentTypeParser');
        it('should exist', () => {
            assert.ok(contentTypeParser);
        });

        describe('.findId', () => {
            it('should exist', () => {
                assert.ok(contentTypeParser.findId);
            });
            it('should get ids from urls ending in article{{id}}.ece', () => {
                assert.equal(contentTypeParser.findId('http://nat-dev-cms-si.mirror.co.uk:8080/mirror/comm-part-test/endava/article3455374.ece'), 3455374);
            });
            it('should get ids from urls ending in -{{id}}', () => {
                assert.equal(contentTypeParser.findId('http://nat-dev-cms-si.mirror.co.uk:8080/mirror/sport/video/test-3586586'), 3586586);
            });
            it('should get ids from urls ending in content/{{id}}', () => {
                assert.equal(contentTypeParser.findId('http://nat-dev-cms-stable.mirror.co.uk:8080/webservice/escenic/content/6590130'), 6590130);
            });

        });

        describe('.parse', () => {
            var buildParseTest = (test) => {
                it('should parse a html ' + test.type, () => {
                    var actual = contentTypeParser.parse(test.input);
                    assert.deepEqual(actual, test.output);
                });
            };
            R.forEach(buildParseTest, tests);
        });
    });

    describe('stringify', () => {
        var stringify = require('../stringify.js');
        it('should exist', () => {
            assert.ok(stringify);
        });

        it("stringifies a basic object", () => {
            assert.equal(stringify('regionals', {
                "foo" : "foo"
            }), '{\"foo\": \"foo"}');
        });

        it("does not add empty values", () =>{
            assert.equal(stringify('regionals', {
                "foo" : "foo",
                "bar" : void 0,
                "gee" : ""
            }), '{\"foo\": \"foo", \"gee\": \"\"}');
        });

        it("handles escenic arrays", () => {
            assert.equal(stringify('regionals',
                [
                    {
                        type: 'escenic',
                        id: 1234
                    }
                ]
            ), '[${/v1/live-centre/regionals-article/1234;null}]');
        });

        it("handles other arrays", () => {
            assert.equal(stringify('regionals',
                [
                    {
                        type: 'paragraph',
                        text: "left as is"
                    }
                ]
            ), '[{\"type\": \"paragraph\", \"text\": \"left as is\"}]');
        });

        // currently fails (returns empty string)
        // it("handles empty arrays", () => {
        //     assert.equal(stringify('regionals', []), void 0);
        // })

        it("handles objects with the $ref prop differently", () => {
            assert.equal(stringify('regionals', {
                "$ref" : "ref_with_dollar_sign",
                "foo" : "bar"
            }), "ref_with_dollar_sign");
        });

        it("returns 'null' if second value is null", () => {
            assert.equal(stringify('regionals', null), "null");
        });

        it("returns a stringified version if not object or array", () => {
            assert.equal(stringify('regionals', 10), "10");
            assert.equal(stringify('regionals', "abc"), '"abc"');
        });

        it('altogether', () => {
                assert.equal(stringify('regionals', {
                    some: "property",
                    nothing: null,
                    foo: "bar",
                    content: [
                        {
                            type: 'escenic',
                            id: 1234
                        },
                        {
                            type: 'paragraph',
                            text: "left as is"
                        }
                    ]
                }), '{'
                        + '\"some\": \"property\",'
                        + ' \"foo\": \"bar\",'
                        + ' \"content\": ['
                            + '${/v1/live-centre/regionals-article/1234;null},'
                            + ' {'
                                + '\"type\": \"paragraph\",'
                                + ' \"text\": \"left as is\"'
                            + '}'
                        + ']'
                    + '}');
            });
    });

    describe('presentation', () => {
        var presentation = rewire('../presentation.js');
        var content;

        describe('getPlatform', () => {
            const getPlatform = presentation.__get__('getPlatform');

            it('should get the platform from the event source', () => {
                assert.strictEqual(getPlatform('nationals-live'), 'nationals');
                assert.strictEqual(getPlatform('regionals-article'), 'regionals');
            })
        });

        describe('createBaseUrlRef', () => {
            const createBaseUrlRef = presentation.__get__('createBaseUrlRef');

            it('should get create the root of the reference for the specified source type', () => {
                assert.strictEqual(createBaseUrlRef('publication', 'nationals'), '${/v1/live-centre/nationals-publication');
                assert.strictEqual(createBaseUrlRef('publication', 'regionals'), '${/v1/live-centre/regionals-publication');
            })
        });

        describe('createSourceBaseUrlRef', () => {
            const createSourceBaseUrlRef = presentation.__get__('createSourceBaseUrlRef');

            it('should return a function', () => {
                assert.isFunction(createSourceBaseUrlRef('publication'));
            })
        });

        describe('createSectionBaseUrlRef', () => {
            const createSectionBaseUrlRef = presentation.__get__('createSectionBaseUrlRef');

            it('should create the root of the reference for the section source type', () => {
                assert.strictEqual(createSectionBaseUrlRef('nationals-live'), '${/v1/live-centre/nationals-section');
                assert.strictEqual(createSectionBaseUrlRef('regionals-live'), '${/v1/live-centre/regionals-section');
            })
        });

        describe('createArticleBaseUrlRef', () => {
            const createArticleBaseUrlRef= presentation.__get__('createArticleBaseUrlRef');

            it('should create the root of the reference for the article source type', () => {
                assert.strictEqual(createArticleBaseUrlRef('nationals'), '${/v1/live-centre/nationals-article');
                assert.strictEqual(createArticleBaseUrlRef('regionals'), '${/v1/live-centre/regionals-article');
            })
        });

        describe('createEscenicReference', () => {
            const createEscenicReference = presentation.__get__('createEscenicReference');

            it('should create references for escenic articles', () => {
                assert.strictEqual(createEscenicReference('nationals', '12345', ['path']), '${/v1/live-centre/nationals-article/12345,path;}');
                assert.strictEqual(createEscenicReference('regionals', '12345', ['path']), '${/v1/live-centre/regionals-article/12345,path;}');
            })
        });

        describe('getMeta', () => {
            it("converts entry to output - also uses buildPresentation()", () => {
                var entry = require('./data.json').liveEventEntry;
                var getMeta = presentation.__get__('getMeta');
                const meta = getMeta(entry);
                assert.equal(meta.id, 8880);
                assert.equal(meta.state, "published");
                assert.equal(meta.sourceType, "live");
                assert.equal(meta.source, "nationals");
                assert.equal(meta.self, "/v1/live-centre/nationals-live/6458476/8880");
                assert.equal(meta.type, "live-event-entry");
                assert.deepEqual(meta._presentation, {
                    isSticky: false,
                    hideTimestamp: false,
                    id: 8880,
                    event: 'nationals-live/6458476',
                    postTime: 1445247817090,
                    createdDate: '2015-10-19T10:43:37.090+01:00',
                    lastModifiedTime: 1445247817090,
                    author: 'Mirror .co.uk',
                    prefix: 'test prefix',
                    title: 'Test title',
                    type: 'default',
                    content: [ { type: 'text', text: 'asdgdghdfhdfh' } ]
                })
            })
        });

        describe('sanitiseHtmlPresentation', () => {
            const sanitiseHtmlPresentation = presentation.__get__('sanitiseHtmlPresentation');

            it('should not change html with no escenic links', () => {
                assert.deepEqual(sanitiseHtmlPresentation('regionals-live', {
                        type: 'html',
                        text: "<p>some text</p><ol><li>One</li><li>Two</li><li>Three</li></ol>"
                    }),
                    {
                        text: "<p>some text</p><ol><li>One</li><li>Two</li><li>Three</li></ol>",
                        type: "html"
                    }
                );

                assert.deepEqual(sanitiseHtmlPresentation('regionals-live', {
                        type: 'html',
                        text: "<p>internal article</p>"
                    }),
                    {
                        text: "<p>internal article</p>",
                        type: "html"
                    }
                );
            });

            it('should replace escenic links in html with appropriate placeholders', () => {
                const html = {
                    type: 'html',
                    text: "<ul><li><a href=\"internal:/article/11116910\" class=\"esc-content esc-tag-escenic-story esc-article-id-11116910\">Manchester United legend Ferguson’s verdict on Manchester City Pep Guardiola coup</a><br /></li><li><a href=\"internal:/article/11117463\" class=\"esc-content esc-tag-escenic-story esc-article-id-11117463\">Nasri fears over Manchester City Champions League hopes </a><br /></li><li><a href=\"internal:/article/11116713\" class=\"esc-content esc-tag-escenic-story esc-article-id-11116713\">Manchester City close in on Barcelona goalkeeper and West Ham want Wilfried Bony - transfer reports</a><br /></li></ul>"
                };
                assert.deepEqual(sanitiseHtmlPresentation('regionals-live', html),
                    {
                        "text": "<ul><li><a href=\"${/v1/live-centre/regionals-article/11116910,url;}\" class=\"esc-content esc-tag-escenic-story esc-article-id-11116910\">Manchester United legend Ferguson’s verdict on Manchester City Pep Guardiola coup</a><br /></li><li><a href=\"${/v1/live-centre/regionals-article/11117463,url;}\" class=\"esc-content esc-tag-escenic-story esc-article-id-11117463\">Nasri fears over Manchester City Champions League hopes </a><br /></li><li><a href=\"${/v1/live-centre/regionals-article/11116713,url;}\" class=\"esc-content esc-tag-escenic-story esc-article-id-11116713\">Manchester City close in on Barcelona goalkeeper and West Ham want Wilfried Bony - transfer reports</a><br /></li></ul>",
                        "type": "html"
                    }
                );
            });
        });

        describe('buildPublicationPresentation', () => {
            var data = require('./data.json'),
                buildPublicationPresentation = presentation.__get__('buildPublicationPresentation');

            it('should exist', () => {
                assert.ok(buildPublicationPresentation);
            });

            it('should return the public domain of the publication', () => {
                assert.deepEqual(buildPublicationPresentation(data.publication), 'http://www.mirror.co.uk');
            });
        });

        describe('buildSectionPresentation', () => {
            var data = require('./data.json'),
                buildSectionPresentation = presentation.__get__('buildSectionPresentation');

            it('should exist', () => {
                assert.ok(buildSectionPresentation);
            });

            it('should return the url of the section, with reference to it\'s parent publication', () => {
                assert.deepEqual(buildSectionPresentation(data.weirdNewsSection), '${/v1/live-centre/nationals-publication/2}/news/weird-news/');
            });

            it('should return the reference to it\'s parent publication for the root section', () => {
                assert.deepEqual(buildSectionPresentation(data.homeSection), '${/v1/live-centre/nationals-publication/2}/');
            });
        });

        describe('buildArticlePresentation', () => {
            var data = require('./data.json'),
                buildArticlePresentation = presentation.__get__('buildArticlePresentation');

            it('should exist', () => {
                assert.ok(buildArticlePresentation);
            });

            it('should return the article\'s url as a paragraph fragment for galleries', () => {
                assert.deepEqual(buildArticlePresentation(data.gallery), {
                    "type": "escenic",
                    "url": "${/v1/live-centre/nationals-section/70}gallery-test-6681553"
                });
            });

            it('should handle images', () => {
                const image = buildArticlePresentation(data.picture);
                assert.strictEqual(image.altText, "Jamie Vardy scores the first goal for Leicester");
                assert.strictEqual(image.caption, "Jamie Vardy scores the first goal for Leicester");
                assert.strictEqual(image.imageCredit, "Action Images via Reuters / Carl Recine");
                assert.strictEqual(image.src, "${/v1/live-centre/nationals-section/70}article7774218.ece/ALTERNATES/s510b/Leicester-City-v-West-Ham.jpg");
                assert.strictEqual(image.type, "image");
            });

            it('should handle html', () => {
                const html = buildArticlePresentation(data.html);
                assert.strictEqual(html.type, 'html');
                assert.strictEqual(html.title, 'United away days');
                assert.strictEqual(html.text, '<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/samuelluckhurst">@samuelluckhurst</a> city away <a href="https://twitter.com/jonnyj2k">@jonnyj2k</a> <a href="http://t.co/zVTm6XfXjx">pic.twitter.com/zVTm6XfXjx</a></p>&mdash; Aaron (@Azzle_Dazzle) <a href="https://twitter.com/Azzle_Dazzle/status/655320742633078785">October 17, 2015</a></blockquote>');
            });

        });

        describe('.buildPresentation', () => {
            var buildPresentation = presentation.__get__('buildPresentation');
            var data = require('./data.json');
            it('should exist', () => {
                assert.ok(buildPresentation);
            });

            it('should get parse the data object', () => {
                content = buildPresentation(data.liveEventEntry);
                assert.ok(content);
            });

            it('should have the correct fields', () => {
                content = buildPresentation(data.liveEventEntry);
                assert.deepEqual(content, {
                    id: 8880,
                    type: 'default',
                    postTime: 1445247817090,
                    createdDate: '2015-10-19T10:43:37.090+01:00',
                    lastModifiedTime: 1445247817090,
                    title: 'Test title',
                    prefix: "test prefix",
                    event: 'nationals-live/6458476',
                    isSticky: false,
                    hideTimestamp: false,
                    author: 'Mirror .co.uk',
                    content: [
                        {
                            type: 'text',
                            text: 'asdgdghdfhdfh'
                        }
                    ]
                });
            });

            it('should correctly fallback for escenic articles', () => {
                content = buildPresentation(data.gallery);
                assert.deepEqual(content, {
                    type: 'escenic',
                    url: '${/v1/live-centre/nationals-section/70}gallery-test-6681553'
                });
            });
        });
    });
});
