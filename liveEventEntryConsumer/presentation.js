"use strict";
var R = require('ramda');
var parse = require('./contentTypeParser').parse;
var tz = require('timezone/loaded');

function getMeta(entry) {
    var sourceInfo = entry.Source.split('-');
    return {
        id: entry.Id,
        _presentation: buildPresentation(entry),
        state: {
            submitted: 'draft',
            created: 'draft',
            accepted: 'published',
            rejected: 'deleted'
        }[entry.State] || entry.State,
        type: entry.ArticleType,
        sourceType: sourceInfo[1],
        source: sourceInfo[0],
        self: '/v1/live-centre/' + entry.Source + '/' + (entry.ArticleType === 'live-event-entry' ? entry.Fields.eventId + '/' + entry.Id : entry.Id)
    };
}

function buildPublicationPresentation (entry) {
    return 'http://' + entry.Features['tm.publication.publicDomain'];
}

// getPlatform :: String -> String
const getPlatform = R.compose(R.head, R.match(/nationals|regionals/));

// createBaseUrlRef :: String -> String -> String
const createBaseUrlRef = R.curry((sourceType, platform) => '${/v1/live-centre/' + platform + '-' + sourceType);

// createSourceBaseUrlRef :: String -> (String -> String)
const createSourceBaseUrlRef = source => R.compose(createBaseUrlRef(source), getPlatform);

// createSectionBaseUrlRef :: String -> String
const createSectionBaseUrlRef = createSourceBaseUrlRef('section');

// createArticleBaseUrlRef :: String -> String
const createArticleBaseUrlRef = createSourceBaseUrlRef('article');

// buildSectionPresentation :: String -> String
function buildSectionPresentation (entry) {
    const baseUrlRef = R.compose(createSourceBaseUrlRef('publication'), R.prop('Source'));
    return baseUrlRef(entry) + '/' + entry.OwnerPublicationId + '}/' + entry.RelativePath.replace(/^\//,'');
}

function buildArticlePresentation (entry) {
    if (entry.ArticleType === 'picture') {
        return {
            type: 'image',
            imageCredit: entry.Fields.photographer || "",
            caption: entry.Fields.caption || "",
            altText: entry.Fields.alttext || "",
            src: createSectionBaseUrlRef(entry.Source) + '/' + entry.OwnerHomeSection + '}' + entry.FileName + '/ALTERNATES/s510b/' + entry.Title
        };
    } else if (entry.ArticleType === 'html') {
        return {
            type: 'html',
            title: entry.Fields.title,
            text: entry.Fields.html
        };
    } else {
        var obj = {
            type: 'escenic',
            url: createSectionBaseUrlRef(entry.Source) + '/' + entry.OwnerHomeSection + '}' + entry.FileName
        };
        if (entry.Type === 'live-event' && entry.Fields && entry.Fields['sub-type'] === 'match' && entry.Fields.fixtureId){
            obj.fixtureId = entry.Fields.fixtureId;
        }
        return obj;
    }
}

// createEscenicReference :: String -> String -> [String] -> String
const createEscenicReference = (source, id, path) => {
    return [createArticleBaseUrlRef(source), id].join('/') + ',' + path.join(',') + ';}';
};

// sanitiseHtmlPresentation :: String -> Object -> Object
const sanitiseHtmlPresentation = (source, content) => {
    const regex = /(internal:\/article\/\d+)/g;
    if(!R.test(regex, content.text)) return content;
    const escenicInternalLinks = R.match(regex, content.text);
    const escenicExternalLinks = R.map(link => {
            const id = R.head(link.split('/').reverse());
            return R.replace(regex, createEscenicReference(source, id, ['url']), link);
        },
        escenicInternalLinks);
    let text = content.text;
    R.forEach(pair => {
        text = text.replace(pair[0], pair[1]);
    }, R.zip(escenicInternalLinks, escenicExternalLinks));
    return R.merge(content, { text });
};

function buildEntryPresentation (entry) {
    var authorArray = R.pluck('Name', entry.Authors);
    return {
        id: entry.Id,
        postTime: entry.PublishTime,
        createdDate: tz(!R.isNil(entry.PublishTime) ? entry.PublishTime : entry.CreationTime, '%Y-%m-%dT%H:%M:%S.%3N%:z', 'Europe/London'),
        lastModifiedTime: entry.LastModifiedTime,
        type: (entry.Fields.entryStyle || '').replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase(), // camel-case to kabab-case
        title: entry.Fields.title,
        hideTimestamp: entry.Fields.hideTimestamp === 'true',
        isSticky: entry.Fields.sticky,
        author: R.isEmpty(authorArray) ? void 0 : authorArray.join(','), // optional
        prefix: entry.Fields.prefix,
        event: [entry.Source, entry.Fields.eventId].join('/'),
        content: parse(entry.Fields.body)
            .map(function (content) {
                switch (content.type) {
                    case 'html':
                        return sanitiseHtmlPresentation(entry.Source, content);
                    case 'image':
                        return R.merge(content, {
                            imageCredit: createEscenicReference(entry.Source, content.id, ['imageCredit']),
                            url: createEscenicReference(entry.Source, content.id, ['src'])
                        });
                    default:
                        return content;
                }
            })
    };
}

function buildPresentation(entry) {
    if (~entry.Source.indexOf('-live')) {
        return buildEntryPresentation(entry);
    } else if (~entry.Source.indexOf('-article')) {
        return buildArticlePresentation(entry);
    } else if (~entry.Source.indexOf('-section')) {
        return buildSectionPresentation(entry);
    } else if (~entry.Source.indexOf('-publication')) {
        return buildPublicationPresentation(entry);
    } else {
        return {};
    }
}

module.exports = {
    getMeta: getMeta
};